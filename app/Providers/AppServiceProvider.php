<?php

namespace App\Providers;

use Request;
use App\Models\Category;
use App\Models\Location;
use App\Models\Announcement;
use Illuminate\Support\Facades\App;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {


        // se non esiste una sessione
        // nel senso che un utente non si è mai collegato al sito
        // o non si è collegato di recente
        // viene settato il linguaggio di default del sistema


        $locale=session('locale');
      
    
        if ($locale == null)
        {
            // VIENE LETTO LA LINGUA DI DEFAULT DEL SISTEMA/BROWSER
            // questi controlli sono necessari perché non è detto che il 
            // server fornisca i dati di $_SERVER
            // in mancanza di diversi indicazioni circa il locale
            // il sistema si setta in automatico in inglese
            if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))
                {
                    $preferred = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"],0,2);
                }
            elseif (null !== (Request::getPreferredLanguage()))
                {
                    $preferred = substr(Request::getPreferredLanguage(),0,2);
                }
            elseif (null !== (Config::get('app.locale')))
            {
                $preferred = Config::get('app.locale');
            }
            else
                {
                    $preferred = 'en';
                }


            // la riga di cui sotto è una alternativa
            // in cui il linguaggio di default viene prelevato
            // dal file config/app.php

            // $$preferred=Config::get('app.locale');
            Session::put('locale', $preferred);

        }

        if(Schema::hasTable('categories'))
        {
            $categories = Category::all();
            View::share('categories', $categories);
        }

        if(Schema::hasTable('locations'))
        {
            $locations = Location::all();
            View::share('locations', $locations);
        }

        $etichette = ['Adult', 'Spoof', 'Medical', 'Violence', 'Racy'];
        View::share('etichette', $etichette);
        
        Paginator::useBootstrap();

        // if(Schema::hasTable('announcements'))
        // {
        //   $announcements=Announcement::all();  
        //   View::share('announcements',$announcements);
        // }

    }
}
