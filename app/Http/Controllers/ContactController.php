<?php

namespace App\Http\Controllers;

use App\Mail\MailContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{

    public function send(Request $request)
    {
        $name = $request->input('name_contatto');
        $email = $request->input('email_contatto');

        $informazioni = compact('name', 'email');
     

        Mail::to('administration@presto.com')->send(new MailContact($informazioni));

        return $this->thankyou($informazioni);
    }

    public function thankyou($informazioni)
    {
        
        return view('components.thankyou', compact('informazioni'));
    }
}
