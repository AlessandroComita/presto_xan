<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Http\Requests\AnnouncementRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */ 
    public function index()
    {
        $announcements=Announcement::all();  
        return view('home', compact('announcements'));    

    }

    public function newAnnouncement(Request $request)
    {        
        // se in old il sistema ha già una variabile che si chiama uniqueSecret
        // prende il valore old,
        // altrimenti calcola un nuovo uniqueSecret
        $uniqueSecret = $request->old
        (
            'uniqueSecret', 
            base_convert(sha1(uniqid(mt_rand())), 16, 36)
        );
        return view('announcements.new', compact('uniqueSecret'));
    }

    public function createAnnouncement(AnnouncementRequest $request)
    {
        $a = new Announcement();
        
        $a->title = $request->input('title');
        $a->body = $request->input('body');
        $a->price = $request->input('price');
        $a->category_id = $request->input('category');
        $a->location_id = $request->input('location');
        $a->user_id = Auth::id();

        $a->save();

        $uniqueSecret = $request->input('uniqueSecret');

        // dalla sessione, leggi elenco immagini aggiunte in componente Dropzone
        // il secondo parametro indica che di default, 
        // se non dovesse esserci una entry in sessione, imposta un array vuoto
        $images = session()->get("images.{$uniqueSecret}", []);
  
        $removedImages = session()->get("removedimages.{$uniqueSecret}", ['vuoto']);

        // in un nuovo elenco, mettiamo la differenza tra immagini inserite e rimosse
        $images = array_diff($images, $removedImages);
        
        // procedura di salvataggio per ogni immagine 
        foreach ($images as $image)
        {
            $i = new AnnouncementImage();

            // la funzione basename permette di catturare solo il nome dell'immagine
            $fileName = basename($image);

            // spostare l'immagine dalla cartella temporanea alla cartella creata con id e file name
            $newFileName = "public/announcements/{$a->id}/{$fileName}";
            Storage::move($image, $newFileName);  

            dispatch(new ResizeImage
            (
                $newFileName,
                300,
                200
            ));

            dispatch(new ResizeImage
            (
                $newFileName,
                400,
                300
            ));


            $i->file = $newFileName;
            $i->announcement_id = $a->id;

            $i->save();


 
            // GoogleVisionSafeSearchImage::withChain([
            //     // new GoogleVisionLabelImage($i->id),
            //     // new GoogleVisionRemoveFaces($i->id),
            //     new ResizeImage ($i->file, 300, 200),
            //     new ResizeImage($i->file, 400, 300)

            // ])->dispatch($i->id);

            // new ResizeImage ($i->file, 300, 200);
            // dispatch($i->id);
                


        }

        // a questo punto, la cartella temporanea non serve più, quindi si può cancellare
        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));


        return redirect('/home')->with('announcement.created.success','ok');
    }




    public function uploadImage(Request $request)
    {
        // catturiamo il "segreto" e lo mettiamo in una variabile
        $uniqueSecret = $request->input('uniqueSecret');

        // utilizziamo le potenzialità di Lavarel Storage per memorizzare su disco
        // il file immagine che è presente nella request
        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

   
        dispatch(new ResizeImage
        (
            $fileName,
            120,
            120
        ));


        // push accoda in un elenco
        session()->push("images.{$uniqueSecret}", $fileName);

        // aggiungiamo il nome file appena memorizzato in una variabile di sessione
        // che ha come nome <<images + "il segreto">>
        // dd(session()->get("images.{$uniqueSecret}"));

        return response()->json
        (
            // session()->get("images.{$uniqueSecret}")
            [
                'id' => $fileName
            ]
        );
    }

    public function removeImage(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->input('id');


        // costruiamo un nuovo elemento, un nuovo array in sessione
        // removedImages.segreto
        // in cui andiamo a inserire tutte le immagini che l'utente vuole cancellare
        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);

        return response()->json('ok');
    }

    // metodo che viene richiamato nel caso c'è un errore di digitazione
    // ovvero: conserva le immagini inserite qualora l'inserimento dell'annuncio viene 
    // bloccato perché non passa la validazione sugli altri campi dell'annuncio
    public function getImages(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedImages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        $data = [];

        foreach ($images as $image)
        {
            $data[] = 
            [
                'id' => $image,
                'src' => AnnouncementImage::getUrlByFilePath($image, 120, 120)
            ];
        }

        return response()->json($data);

    }
}
