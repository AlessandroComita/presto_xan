<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;




class PublicController extends Controller
{


    public function index()
    {
        // $announcements=Announcement::all()->take(5);
        $announcements=Announcement::orderBy('created_at','desc')->where('is_accepted','<>',0)->take(5)->get();

        return view('home', compact('announcements'));    
    }




    public function announcementsByCategory ($name, $category_id)
    {
        $category = Category::find($category_id);
        $announcements = $category->announcements()->orderBy('created_at', 'desc')->paginate(5);
        return view('announcements.announcements', compact('category', 'announcements'));      
    }

    public function locale($locale)
    {
        session()->put('locale', $locale);       
        return redirect()->back();
    }


    public function search(Request $request)
    {
        $q = $request->input('q');
        $price_search = 0;
        $category_search = 0;
        $location_search = 0;
        $announcements = Announcement::search($q)->where('is_accepted', true)->get();

        return view('announcements.search_results', compact('q', 'announcements', 'price_search', 'category_search', 'location_search'));
    }
    

    public function filtered_search(Request $request)
    {
        $q = $request->input('q');
        $category_search = $request->input('category');

        $price_search = $request->input('price_search');
        $location_search = $request->input('location');

        
        $announcements = Announcement::search($q)->where('is_accepted', true)->get();

        

        



        $filtered = [];
        foreach ($announcements as $announcement)
        {
            

            if 
            (
                (
                    $announcement->price <= $price_search 
                    || $price_search == 0
                    || $price_search == 9999
                )
            
            && 
            
                (
                    $announcement->category_id == $category_search || $category_search == 0
                )

            &&

                (
                    $announcement->location_id == $location_search || $location_search == 0
                )


            ) 
            {
                $filtered[] = $announcement;
            }
        }
        


        $announcements = $filtered;

        return view('announcements.search_results', compact('q', 'announcements', 'price_search', 'category_search', 'location_search'));
    }



    public function detail($id)
    {
            $announcement = Announcement::find($id);
        return view('announcements.detail', compact('announcement'));
    }
}
