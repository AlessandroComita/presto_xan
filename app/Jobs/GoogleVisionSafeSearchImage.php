<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Models\AnnouncementImage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

class GoogleVisionSafeSearchImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */


    private $announcement_image_id;

    public function __construct($announcement_image_id)
    {
        $this->announcement_image_id = $announcement_image_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // carico il modello AnnouncementImage e lo metto in una variabile
        $i = AnnouncementImage::find($this->announcement_image_id);

        // se il modello non esiste, cioè se ad esempio ho ricevuto un ID che
        // poi è stato cancellato o se per qualche ragione non è corretto,
        // esci senza fare nulla
        if (!$i) { return; }

        // se invece esiste, leggo il contenuto del file che è stato
        // indicato come immagine, leggendo l'immagine originaria
        // quella a definizione massima
        // prendo il contenuto dell'immagine e lo metto in una variabile
        $image = file_get_contents(storage_path('/app/' . $i->file));

        // da php, settare una variabile d'ambiente che servirà alla libreria Google
        // vado ad inserire il percorso del file che contiene la mia chiave privata, le credenziali
        // «base_path» è una funzione di utilità Laravel che mi restiusce il percorso del mio progetto, in questo caso il percorso completo del file che si trova nella root del mio progetto, file che si chiama «google_credential.json»
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . base_path('google_credential.json'));

        // l'oggetto che mi viene fornito dalla libreria Google.
        // All'interno quell'oggetto andrà a cercare, attraverso la variabile d'ambiente, le credenziali per il collegamento
        $imageAnnotator = new ImageAnnotatorClient();

        // inizializzo il client che mi serve
        // andiamo a richiedere un safeSearchDetection su questa immagine
        $response = $imageAnnotator->safeSearchDetection($image);
        $imageAnnotator->close();

        // recuperiamo la risposta di Google e da quella risposta di Google
        $safe = $response->getSafeSearchAnnotation();

        // andiamo a prendere le annotazioni, annotazioni che mi daranno un indice che va da zero a cinque
        $adult = $safe->getAdult();
        $medical = $safe->getMedical();
        $spoof = $safe->getSpoof();
        $violence = $safe->getViolence();
        $racy = $safe->getRacy();

        // visualizzare i valori catturati per capire cosa sta succedendo
        // è una istruzione di appoggio per visualizzare i dati via tinker
        // perciò è commentata
        // echo json_encode([$adult, $medical, $spoof, $violence, $racy]);

        // andiamo a creare un array che rappresenta le etichette del dato che Google mi sta restituendo
        $likelihoodName = 
        [
            'UNKNOWN', 'VERY_UNLIKELY', 'UNLIKELY', 'POSSIBLE', 'LIKELY', 'VERY_LIKELY'
        ];

        $i->adult = $likelihoodName[$adult];
        $i->medical = $likelihoodName[$medical];
        $i->spoof = $likelihoodName[$spoof];
        $i->violence = $likelihoodName[$violence];
        $i->racy = $likelihoodName[$racy];

        $i->save();
        




    }
}
