<?php

namespace App\Jobs;

use Spatie\Image\Image;
use Illuminate\Bus\Queueable;
use Spatie\Image\Manipulations;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ResizeImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $path, $fileName, $w, $h;

    public function __construct($filePath, $w, $h)
    {
        // dividiamo in due parti il filepath

        // il solo file name
        $this->path = dirname($filePath);

        // il percorso che precede il filename
        $this->fileName = basename($filePath);

        // memorizziamo larghezza e altezza delle immagini
        $this->w = $w;
        $this->h = $h;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $w = $this->w;
        $h = $this->h;
        
        // andiamo a costruire il path sorgente, di origine
        // che è dato dalla prima parte più la seconda 
        // all'interno dello storage_path
        $srcPath = storage_path() . '/app/' . $this->path . '/' . $this->fileName;

        // poi definiamo il path destinazione
        // che è composto dal path iniziale uguale
        // e poi il nome del file sarà prefisso con la scritta crop+larghezza+altezza
        // ad esempio, se l'immagine si chiama "pippo"
        // questo job creerà una nuova immagine che si chiamerà crop10x30_pippo
        // così da affianchare l'immagine originale alle immagini ridimensionate
        $destPath = storage_path() . '/app/' . $this->path . "/crop{$w}x{$h}_" . $this->fileName;

        // esegue effettivamente il crop partendo dal centro della immagine
        // mantenendo le proporzioni 
        Image::load($srcPath)
 

            
            ->crop(Manipulations::CROP_CENTER, $w, $h)

            ->watermark(public_path('/img/watermark.png'))
            ->watermarkOpacity(50)
            ->watermarkPadding(20) // 20px padding on all edges
            ->watermarkPosition(Manipulations::POSITION_BOTTOM_LEFT)      // Watermark at the top
            ->watermarkHeight(50, Manipulations::UNIT_PERCENT)    // 30 percent height
            ->watermarkWidth(50, Manipulations::UNIT_PERCENT)    // 30 percent width


            ->save($destPath);
    }
}
