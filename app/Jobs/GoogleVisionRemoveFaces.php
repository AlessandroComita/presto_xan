<?php

namespace App\Jobs;

use Spatie\Image\Image;
use Spatie\Image\Manipulations;

use Illuminate\Bus\Queueable;

use App\Models\AnnouncementImage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

class GoogleVisionRemoveFaces implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $announcement_image_id;


    public function __construct($announcement_image_id)
    {
        $this->announcement_image_id = $announcement_image_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $i = AnnouncementImage::find($this->announcement_image_id);
        if (!$i)
        {
            return;
        }

        $srcPath = storage_path('/app/' . $i->file);
        $image = file_get_contents($srcPath);

        // imposta la variabile di ambiente GOOGLE_APPLICATION_CREDENTIALS
        // al path del credentials file
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . base_path('google_credential.json'));

        $imageAnnotator = new ImageAnnotatorClient();
        $response = $imageAnnotator->faceDetection($image);
        $faces = $response->getFaceAnnotations();

        foreach ($faces as $face)
        {
            // raccolta di tutti i vertici
            $vertices = $face->getBoundingPoly()->getVertices();

            $bounds = [];

            // ciclo su tutti i vertici
            foreach ($vertices as $vertex)
            {
                // per ogni vertice, inserisce in $bounds un ulteriore array 
                // che contiene due elementi
                // la x e la y
                $bounds[] = [$vertex->getX(), $vertex->getY()];
            }

            // calcola larghezza e altezza volto
            $w = $bounds[2][0] - $bounds[0][0];
            $h = $bounds[2][1] - $bounds[0][1];

            // tramite la libreria spatie, ricarica immagine sorgente
            $image = Image::load($srcPath);

            // applicare una serie di mutazioni, nel dettaglio andremo ad inserire il watermark
            $image->watermark(base_path('resources/img/smile.png'))
                ->watermarkPosition('top-left')
                ->watermarkPadding($bounds[0][0], $bounds[0][1])
                ->watermarkWidth($w, Manipulations::UNIT_PIXELS)
                ->watermarkHeight($h, Manipulations::UNIT_PIXELS)
                ->watermarkFit(Manipulations::FIT_STRETCH);

            $image->save($srcPath);

            
            // echo "face\n";
            // foreach ($vertices as $vertex)
            // {
            //     echo $vertex->getX() . ", " . $vertex->getY() . "\n";
            // }
        }

        $imageAnnotator->close();   
    }
}
