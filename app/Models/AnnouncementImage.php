<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AnnouncementImage extends Model
{
    use HasFactory;

    // si dice a Laravel di interpretare il campo labels come un array
    // sono gli eloquent mutators
    protected $casts =
    [
        'labels' => 'array',
    ];


    public function announcement()
    {
        return $this->belongsTo(Announcement::class);
    }


    // larghezza e altezza sono opzionali:
    // se sono null, significa che voglio visualizzare l'immagine con dimesioni originali


    static public function getUrlByFilePath($filePath, $w = null, $h = null)
    {
        if (!$w && !$h)
        {
            // se larghezza e altezza sono presenti
            // andiamo a scomporre il filepath in due parti
            return Storage::url($filePath);
        }

        // una prima parte che conterrà la directory
        $path = dirname($filePath);

        // una seconda parte che contiene il nome del file
        $filename = basename($filePath);

        // ricostruiamo il nuovo percorso
        $file = "{$path}/crop{$w}x{$h}_{$filename}";

        return Storage::url($file);
    }

    // se abbiamo un oggetto AnnouncementImage possiamo usare questo metodo
    public function getUrl($w = null, $h = null)
    {
        // rimandiamo al metodo precedente 
        return AnnouncementImage::getUrlByFilePath($this->file, $w, $h);
    }
}
