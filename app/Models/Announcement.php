<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Announcement extends Model
{
    use HasFactory;
    use Searchable;

    public function toSearchableArray()
    {
        $array = 
        [   
            // è obbligatorio inserire l'id fra i campi da indicizzare
            'id'=>$this->id,  

            'title'=>$this->title,
            'body'=>$this->body,
            'altro'=>'annunci',
            // specifico al sistema di indicizzare nome e corpo dell'annuncio

            // tramite l'attributo "altro"
            // cerca una stringa generica
            // cioè, se cerco "annunci"
            // vedrò tutti gli annunci
        ];   

        return $array;
    }


    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function images()
    {
        return $this->hasMany(AnnouncementImage::class); 
    }


    static public function ToBeRevisionedCount()
    {
        return Announcement::where('is_accepted', null)->count();
    }

    
    
}
