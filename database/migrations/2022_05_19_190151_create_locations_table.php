<?php

use App\Models\Location;
use Illuminate\Support\Facades\Schema;
use function Symfony\Component\String\b;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();

            $table->text('continent')->nullable();

            $table->timestamps();
        });


        $continents = 
        [
            'Africa', 'Asia', 'Europe', 'North America', 
            'South America', 'Antarctica', 'Australia'
        ];

        $indice = 0;
        foreach ($continents as $continent) {
            $c = New Location();
            $c->continent = $continent;
            $c->save();
            $indice++;
        }

    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
};
