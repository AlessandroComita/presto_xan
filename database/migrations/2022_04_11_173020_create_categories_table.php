<?php

use App\Models\Category;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();

            $table->string('name')->nullable();
            $table->string('nome_it')->nullable();
            $table->string('nome_es')->nullable();
            $table->string('path')->nullable();

            $table->timestamps();
        });


        $categories = 
        [
            'Videogames', 'Electronics', 'Sport', 'Welness', 'Clothing',
            'Real estate', 'Books', 'Movies', 'Motors'
        ];

        $categorie = 
        [
            'Videogames', 'Elettronica', 'Sport', 'Benessere', 'Abbigliamento',
            'Immobili', 'Libri', 'Film', 'Motori'
        ];

        $categorias = 
        [
            'Videojuegos', 'Electrónica', 'Deporte', 'Bienestar', 'Ropa',
            'Bienes raíces', 'Libros', 'Películas', 'Motores'
        ];

        $path =
        [
            '/img/categories/videogames.jpg',
            '/img/categories/electronics.jpg',
            '/img/categories/sports.jpg',
            '/img/categories/wellness.jpg',
            '/img/categories/clothing.jpg',
            '/img/categories/realestate.jpg',
            '/img/categories/books.jpg',
            '/img/categories/movies.jpg',
            '/img/categories/motors.jpg'
        ];

        $indice = 0;
        foreach ($categories as $category) {
            $c = New Category();
            $c->name = $category;
            $c->nome_it = $categorie[$indice];
            $c->nome_es = $categorias[$indice];
            $c->path = $path[$indice];
            $c->save();
            $indice++;
        }

        // $c_it = Category::all();
        // $indice = 0;

        // foreach ($categorie as $categoria) {
            
        //     $c_it[$indice]->nome_it = $categoria;
        //     $c_it[$indice]->save();
        //     $indice++;
        // }

        // $c_it = Category::all();
        // $indice = 0;

        // foreach ($categorias as $categoria) {
            
        //     $c_it[$indice]->nome_es = $categoria;
        //     $c_it[$indice]->save();
        //     $indice++;
        // }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
};
