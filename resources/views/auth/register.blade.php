<x-layout>
    
   
    <div class="container" style="margin-top: 110px;">
        <div class="row justify-content-center ">
            <div class="col-12 col-md-5  ">


                <div class="container-md     mt-5 bg-charcoal rounded-1rem shadow1px  ">
    

                    {{-- riga intestazione --}}




                    <div class="row    mb-2">
                        
                        <div class="col-12 text-center maize-crayola display-5">
                            <img src="/img/watermark.png"
                                class="pt-3" 
                                style="width: 100px;">
                        </div>
                    </div>


                    <div class="row    mb-2">
                        <div class="col-12 text-center maize-crayola display-5">
                                    Register
                        </div>
                    </div>
                
            
                    {{-- riga contenuto form  --}}
                    <div class="row   mb-2 ps-2 pe-2 pb-5 pt-3">
                     
            
            
                        <div class="col-12  ">
            

                            

                            <form method="POST" action="{{ route('register') }}">
                                @csrf
        
                                <div class="row align-items-end">  
                                    <label for="name" class="py-0 px-2 m-0  col-form-label text-start d-flex align-items-end maize-crayola">{{ __('Name') }}</label>
                                </div>
        
                                <div class="row mb-3">
                                    <div class="col-12 m-0 py-0 px-2">  
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                                        
                                        style="border: 1px solid var(--maize-crayola)"
                                        >
        
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                          
                                <div class="row align-items-end">  
                                    <label for="email" class="py-0 px-2 m-0  col-form-label text-start d-flex align-items-end maize-crayola">{{ __('Email Address') }}</label>
                                </div>
        
                                <div class="row mb-3">
                                    <div class="col-12 m-0 py-0 px-2"> 
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"
                                        
                                        style="border: 1px solid var(--maize-crayola)"
                                        
                                        >
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            
        
                                <div class="row  align-items-end"> 
                                    <label for="password" class="py-0 px-2 m-0  col-form-label text-start d-flex align-items-end maize-crayola">{{ __('Password') }}</label>
                                </div>
        
                                <div class="row mb-3">
                                    <div class="col-12 m-0 py-0 px-2"> 
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"
                                        
                                        style="border: 1px solid var(--maize-crayola)"
                                        
                                        >
        
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                               
        
                                <div class="row  align-items-end"> 
                                    <label for="password-confirm" class="py-0 px-2 m-0  col-form-label text-start d-flex align-items-end maize-crayola">{{ __('Confirm Password') }}</label>
                                </div>
        
                                <div class="row mb-3">
                                    <div class="col-12 m-0 py-0 px-2"> 
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password"
                                        
                                        style="border: 1px solid var(--maize-crayola)"
                                        
                                        >
                                    </div>
                                </div>
        
                                <div class="row mb-0 mt-5 ">
                                    <div class=" col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn bg-burnt-sienna text-white w-100">
                                            {{-- {{ __('Register') }} --}}
                                            Enter
                                        </button>
                                    </div>
                                </div>
                            </form>
                             
                                    
                         
            
            
            
                        </div>
                    </div>
            
            
            
            
            
                </div>




            </div>
        </div>
    </div>



    
        
        
        
        
        
        
    </x-layout>