<x-layout>
    
   
    <div class="container" style="margin-top: 110px;">
        <div class="row justify-content-center ">
            <div class="col-12 col-md-5  ">


                <div class="container-md     mt-5 bg-charcoal rounded-1rem shadow1px  ">
    

                    {{-- riga intestazione --}}
                    
                    
                    <div class="row    mb-2">
                        
                        <div class="col-12 text-center maize-crayola display-5">
                            <img src="/img/watermark.png"
                                class="pt-3" 
                                style="width: 100px;">
                        </div>
                    </div>
                    
                    
                    <div class="row    mb-2">
                        <div class="col-12 text-center maize-crayola display-5">
                                    Login 
                        </div>
                    </div>
                
            
                    {{-- riga contenuto form  --}}
                    <div class="row   mb-2 ps-2 pe-2 pb-5 pt-3">
                     
            
            
                        <div class="col-12  ">
            
                             
                                    
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
            
                                <div class="row  align-items-end">
                                    <label for="email" class="  py-0 px-2 m-0  col-form-label text-start d-flex align-items-end maize-crayola">{{ __('Email Address') }}</label>
                                </div>
            
                                <div class="row mb-3">
                                    <div class="col-12 m-0 py-0 px-2">
                                        <input id="email" type="email" class="form-control input-form @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

      
            
                                <div class="row align-items-end">         
                                    <label for="password" class=" py-0 px-2 m-0  col-form-label text-start d-flex align-items-end maize-crayola">{{ __('Password') }}</label>
                                </div>
            
                                <div class="row  align-items-end">         
                                    <div class="col-12 m-0 py-0 px-2">
                                        <input id="password" type="password" class="form-control input-form @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
            
                                <div class="row mb-3 p-0  ">
                                    <div class="">
                                        <div class="form-check  d-flex justify-content-start mt-2">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            
                                            <label class="ps-1 form-check-label maize-crayola" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
            
                                <div class="row mb-0 mt-5 ">
                                    <div class=" col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn bg-burnt-sienna text-white w-100">
                                            {{-- {{ __('Login') }} --}}
                                            Enter
                                        </button>
                                    </div>
                                    
                                    <div class=" col-12 d-flex justify-content-end p-0 m-0">
                                        @if (Route::has('password.request'))
                                            <a class="btn maize-crayola" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                </div>
                            </form>
            
            
            
                        </div>
                    </div>
            
            
            
            
            
                </div>




            </div>
        </div>
    </div>



    
        
        
        
        
        
        
    </x-layout>