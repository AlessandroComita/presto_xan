<x-layout>
    
    
    <div class="container   mb-5  rounded-1rem " style="margin-top: 150px; ">
    

        {{-- riga intestazione --}}
        <div class="row  mb-2">
            <div class="col-12 col-md-6 offset-md-6">
                @if (!$announcement)


                     <div class="cartello" id="cartello-no-announcements">
                        <h2 class="titolo2 charcoal " style="z-index: 1;"> 
                            There are no announcements to review
                        </h2>
                    </div>
                @endif  
            </div>
        </div>
    
    @if ($announcement)
        {{-- riga contenuto annuncio --}}
        <div class="row mb-2 ps-2 pe-2 pb-5">
            <div class="col-12 col-md-4 h-25  bg-charcoal text-white mt-2 rounded-1rem pb-4 shadow1px sticky-box">
                <h5 class=" ">Announcement ID {{ $announcement->id }}     </h5>
                
                <h5 class=""><b>Title: {{ $announcement->title }} </b></h5>
                <p class="m-0 p-0">Category: 
                    <a class="link-categorie"
                    href="{{ route('public.announcement.category', 
                        [$announcement->category->name, $announcement->category->id])   }}">
    
    
    
                   
                                        
                                @if (App::currentLocale()=='it')
                                    {{ $announcement->category->nome_it }} 
                                @elseif (App::currentLocale()=='es')
                                    {{ $announcement->category->nome_es }} 
                                @elseif (App::currentLocale()=='en')
                                    {{ $announcement->category->name }} 
                                @else
                                    {{ $announcement->category->name }} 
                                @endif
                            
                        
                    </a>  


                </p>



                <p class="">Description: {{ $announcement->body }}</p>
                <h6 class="m-0">Author: {{ $announcement->user->name }}</h6>
                <span class="testo-data-ora m-0">Posted on: {{ $announcement->created_at->format('d/m/y') }}, </span>
                <span class="testo-data-ora m-0"> {{ $announcement->created_at->format('H:i:s') }}</span>


                <div class="col-12   p-0 pt-2 d-flex justify-content-between">
                    <form action="{{ route('revisor.reject',
                    $announcement->id) }}" method="POST">
                        @csrf
                        <button type="submit"
                            class="btn bottone-risposta  d-flex justify-content-center align-items-center  bg-burnt-sienna">
                        
                            <i class="bi bi-x-circle icona-risposta text-white d-flex justify-content-center align-items-center" ></i>
                        </button>
                    </form>

                    <form action="{{ route('revisor.accept',
                    $announcement->id) }}" method="POST">
                        @csrf
                        <button type="submit"
                            class="btn bottone-risposta d-flex justify-content-center align-items-center bg-persian-green p-0">
    
                        <i class="bi bi-check-circle icona-risposta text-white d-flex justify-content-center align-items-center"></i>
                        </button>
                    </form>   

                </div>
           
          


            </div>


            <div class="col-12 col-md-8 ">
               <div class="container p-0 ps-md-2">
                 
                        
                        @foreach ($announcement->images as $image)
                            <div class="row mt-2 bg-charcoal rounded-1rem shadow1px  " >
                                <div class="col-12  col-md-8  d-flex align-items-center justify-content-center ">   
                                    <img class="p-0 py-2 w-100 rounded-1rem  "
                                     src="{{ $image->getUrl() }}">

                                </div>


                                {{-- visualizzazione analisi sensibili --}}

                                <div class="col-12 col-md-4 text-white pt-3 d-flex 
                                    flex-column justify-content-start ">

                                    <div class="box-contenuti">
                                        <p id="etichetta-labels" class="mb-0 p-0 m-0"><b>Contents:</b></p>
                                        <p class="m-0">Adult: 
                                            <x-rating :rate="$image->adult" /> 
                                        </p>      
                                        <p>Spoof:                      
                                                <x-rating :rate="$image->spoof" /> 
                                        </p>
                                        <p>Medical: 
                                            <x-rating :rate="$image->medical" />  
                                        </p>
                                        <p>Violence:                        
                                            <x-rating :rate="$image->violence" />  
                                        </p>       
                                        <p>Racy: 
                                            <x-rating :rate="$image->racy" /> 
                                        </p> 
                                    </div>

                                    <div class="mt-2 mt-md-2 mb-2 mb-md-0">
                                        <p id="etichetta-labels" class="mb-0"><b>Labels:</b></p>
                                        @if ($image->labels)
                                            <p>
                                                @foreach ($image->labels as $label)
                                                    <span id="etichette">{{ $label }} | </span>
                                                @endforeach
                                            </p>
                                        @endif
                                                
         

                                    </div>


                                </div>

                                {{-- visualizzazione etichette --}}



                            </div>
                       @endforeach


               </div>
            </div>
        </div>



        {{-- riga tasti approva e rifiuta --}}

        
        {{-- <div class="row  pb-2 mb-2 mt-1 p-0 justify-content-center align-items-center" >



                      
                       
            <div class="col-6  p-0 d-flex justify-content-end pe-3">
                <form action="{{ route('revisor.reject',
                $announcement->id) }}" method="POST">
                    @csrf
                    <button type="submit"
                        class="btn bottone-risposta  d-flex justify-content-center align-items-center  bg-burnt-sienna">
                    
                        <i class="bi bi-x-circle icona-risposta text-white" ></i>
                    </button>
                </form>
            </div>
       
      
     
            <div class="col-6  p-0 d-flex justify-content-start ps-3">
                <form action="{{ route('revisor.accept',
                $announcement->id) }}" method="POST">
                    @csrf
                    <button type="submit"
                        class="btn bottone-risposta d-flex justify-content-center align-items-center bg-persian-green p-0">

                    <i class="bi bi-check-circle icona-risposta text-white"></i>
                    </button>
                </form>          
            </div>

    
        </div> --}}

    @endif

    </div>
        
        
        
        
        
        
    </x-layout>