<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <div class="cornice" style="background-color: #264653; color: white; padding: 20px;">
        
                <div>
                    <img src="https://i.ibb.co/JpqYyqw/watermark.png"
                        width="100">
                    
                </div>
                <p>Dear Admin</p>
                <p>The user with the following credentials</p>
                <p style="font-size: larger; font-weight: bold;">Name: {{ $informazioni['name'] }} | email: {{$informazioni['email']}}</p>
                <p>applied for the position of revisor.</p>
                <p>You can manage this application through your <a href="http://127.0.0.1:8000/admin/home"
                    style="text-decoration: none;"
                    >
                    <span style="color: #e76f51">                  
                    admin page</span></a>.
                <hr>
                <p>This email was automatically generated.</p>
                <p>Have a nice day.</p>
                <p style="color: #e9c46a; font-weight: bold;">The Presto Company</p>
                


    </div>

</body>
</html>