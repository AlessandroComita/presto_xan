<x-layout>
    
   
  <div class="container mb-5 " style="margin-top: 110px;">
    <div class="row justify-content-center ">
      <div class="col-12 col-md-8  ">


        <div class="container-md     mt-5 bg-charcoal rounded-1rem shadow1px  ">
  

            {{-- riga intestazione --}}

            <div class="row    mb-2">
                  
              <div class="col-12 text-center maize-crayola display-5">
                  <img src="/img/watermark.png"
                      class="pt-3" 
                      style="width: 100px;">
              </div>
            </div>


            <div class="row    mb-2">
                <div class="col-12 text-center maize-crayola display-5">
                            Create Announcement
                </div>
            </div>


          
            {{-- riga contenuto form  --}}
            <div class="row   mb-2 ps-2 pe-2 pb-5 pt-3">
              
    
    
                <div class="col-12  ">
    
                      
                            
                <form action="{{ route('announcement.create') }}"
                  method="POST">
                  @csrf

                  <input 
                    type="hidden"
                    name="uniqueSecret"
                    value="{{ $uniqueSecret }}">


        
                  {{-- campo selezione categoria --}}

                  <div class="row  mb-3" >

                    <div class="col-12 col-sm-6 d-flex align-items-center">
                      <label for="category" class="text-md-right pe-3 maize-crayola">
                        
                          Category

                      </label>
                      <select class="pt-1 d-flex align-items-center justify-content-center rounded"
                        style="border: 1px solid var(--maize-crayola);"
                       name="category" id="category">
                        @foreach ($categories as $category)
                          <option style="vertical-align: middle; padding: 0px !important;
                            font-family: monospace !important;
                          "
                            value="{{ $category->id }}"
                          {{old('category') == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>
                        @endforeach
                      </select>

                      @error('category')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }} </strong>
                        </span>
                      @enderror


                    </div>
                  </div>




                  <div class="mb-3">
                    <label for="title" class="form-label maize-crayola">
                      
                        Title

                    </label>

                    <input id="title" type="text" class="form-control
                    @error('title') is-invalid @enderror"
                    name="title" value="{{ old('title') }}"
                    required autofocus style="border: 1px solid var(--maize-crayola)">

                      @error('title')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }} </strong>
                      </span>
                      @enderror
                  </div>


                    
                  <div class="mb-3">
                    <label for="body" class="form-label maize-crayola">
                      
                      Description

                    </label>

                      <textarea
                          id="body"
                          class="form-control 
                          @error('body') is-invalid @enderror"
                          name="body" required autofocus
                          style="border: 1px solid var(--maize-crayola)"
                          cols="30" rows="10">{{ old('body') }}</textarea>



                    @error('body')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }} </strong>
                      </span>
                    @enderror
                  </div>

                  <div class="mb-3">
                    <label for="price" class="form-label maize-crayola">
                      
                      Price ($)

                    </label>

                    <input id="price" type="number" 
                    name="price" value="{{ old('price') }}"
                    max="99999999999" 
                    min="0"
                    step="0.01"
                    style="border: 1px solid var(--maize-crayola)"
                    class="form-control
                    @error('price') is-invalid @enderror"
                      
                    required autofocus>

                      @error('price')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }} </strong>
                      </span>
                      @enderror
                  </div>


                  {{-- elemento componente dropzone --}}

                  <div class="mb-3">
                    <label for="images" class="form-label maize-crayola">
                      
                      Pictures

                    </label>
                      <div class="dropzone" id="drophere"
                      style="border: 1px solid var(--maize-crayola); border-radius: 5px;"
                      ></div>
                      
                    

                        @error('images')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }} </strong>
                          </span>
                        @enderror
                  </div>

                  {{--  location selector --}}
                  <div class="col-12 col-sm-6 d-flex align-items-center">
                    <label for="location" class="text-md-right pe-3 maize-crayola">
                      
                        Location

                    </label>
  

                      <select class="pt-1 d-flex align-items-center justify-content-center rounded"
                        style="border: 1px solid var(--maize-crayola);"
                        name="location" id="location">

                      @foreach ($locations as $location)
                        <option style="font-family: monospace !important;" value="{{ $location->id }}"
                        {{old('location') == $location->id ? 'selected' : ''}}>{{ $location->continent }}</option>
                      @endforeach
                    </select>

                    @error('category')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }} </strong>
                      </span>
                    @enderror
                  </div>


                  <div class="row mb-0 mt-5 ">
                    <div class=" col-12 d-flex justify-content-center">
                        <button type="submit" class="btn bg-burnt-sienna text-white w-100">
                            Post
                        </button>
                    </div>
                  </div>

                </form>



            {{-- chiude colonna contenuto form --}}
            </div>

            
          {{-- chiude riga contenuto form --}}
          </div>





          
          
          
        {{-- chiude container interno --}}
        </div>




          </div>
      </div>
  </div>



  
      
      
      
      
      
      
  </x-layout>