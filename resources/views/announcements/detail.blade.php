<x-layout>
    
    <div 
        id="container-dettaglio"
        class="container-fluid " style="margin-top: 110px; ">
        
        
        <div class="row ">
            <div class="col-12 ">
                
                
                
                <div class="container  my-5 py-md-5 pt-0 pb-2 px-3 px-md-5" 
                     style="border: 1px solid var(--maize-crayola); border-radius: 15px; background-image: linear-gradient(to bottom, rgba(255,255,255,0.6), rgba(255,255,255,0.3));">
                
                    <div class="row  pb-3 px-0"
                            style="border-bottom: 1px solid var(--persian-green);"
                            >
                        <div 
                                id="box-carosello-dettaglio"
                                class="col-12 col-md-8  p-0 d-flex justify-content-center"
                                style="border-right: 1px solid var(--persian-green);">
                            <x-carousel 
                            :announcement='$announcement'/>
                        </div>

                        {{-- colonna dati annuncio --}}
                        <div 
                            class="ps-3 col-12 col-md-4 d-flex flex-column justify-content-start"
                            
                            >

                            <p class="charcoal mt-2  m-0 p-0 pb-md-5 "        
                                style= "font-size: 1.3rem; font-weight:bold;">
                                    {{ $announcement->title }} 
                            </p>

                            <p class="d-flex justify-content-between display-4"> 
                                <nobr>{{ $announcement->price }} $</nobr>
                            </p>
                            <p class="d-flex align-items-center">
                                <i class="bi bi-tag-fill persian-green icona-dato d-flex align-items-center"></i>
                                <span class="charcoal pt-md-2 ps-2 dettaglio-dato">{{ $announcement->category->name }} </span>
                            </p>

                            <p class="d-flex align-items-center">
                                <i class="bi bi-person-square persian-green icona-dato d-flex align-items-center"></i>
                                <span class="charcoal pt-md-2 ps-2 dettaglio-dato">{{ $announcement->user->name }} </span>
                            </p>

                            <p class="d-flex align-items-center">
                                <i class="bi bi-geo-alt-fill persian-green icona-dato d-flex align-items-center"></i>
                                <span class="charcoal pt-md-2 ps-2 dettaglio-dato">
                                    {{ $announcement->location->continent }}
                                </span>
                            </p>


                            <p class="d-flex align-items-center">
                                <i class="bi bi-calendar-event-fill persian-green icona-dato d-flex align-items-center"></i>
                                <span class="charcoal pt-md-2 ps-2 dettaglio-dato">
                                    {{ $announcement->created_at->format('d/m/y') }}
                                </span>
                            </p>





                            
                            <div class="text-center mb-2 h-100 d-flex align-items-center justify-content-center"
                                            
                            >
                                <button
                                    class="rounded text-white p-2 bottone"
                                    style="background-color: var(--persian-green);"
                                >CONTACT USER</button>
                            </div>
                        </div>

                    </div>  
                    
                    <div class="row">
                        <div class="col-12">
                            <p class="charcoal m-0 p-0 pt-md-2 pt-4 "
                    
                            style= "font-size: 1.1rem;   text-align: justify;
                            text-justify: inter-word;"
                            >{{ $announcement->body }}</p>
                        </div>
                    </div>

                </div>   
            </div>
        </div>
    </div>
</x-layout>