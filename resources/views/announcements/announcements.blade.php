
<x-layout>
    
    <div class="container  h-100 " style=" margin-top:80px; min-height: 1000px;">
        <div class="row ">

            <div class="col-12 col-md-4      pb-2 ">
                <div id="box-ricerca" 
                class="bg-charcoal sticky-box  text-white px-3 pb-4 pt-4 mt-1  mb-md-0"
                style="border-radius: 15px; box-shadow: 5px 5px 1px black;"
                >
                
                
                <div class=" persian-green ps-2 text-center "
                    style="border-radius: 15px; background-color: black;">Search results for <span class="burnt-sienna">{{$category->name}} </span>
                </div>
                
                
                
                <form action="{{ route('filtered_search') }}" method="GET" 
                class="d-flex  h-100  flex-column justify-content-between w-100">
                @csrf
                
                <div>
                    <input name="q" type="text" value=""
                    class="w-100 mb-2 mt-2 input-form" placeholder="Search"></input>
                </div>
                
                <div>
                    <select class="w-100 input-form mb-2" name="category" id="category"
                    style="height: 32px;">
                    
                        <option style="border: 1px solid var(--maize-crayola);" 
                            class=""
                            value="0" selected>Select a category</option>
       
                                        
                        @foreach ($categories as $cat)                           
                                <option style="border: 1px solid var(--maize-crayola);" 
                                value="{{ $cat->id }}"
                                    {{($cat->id == $category->id) ? 'selected' : ''}}>
                                        {{$cat->name}}
                
                                </option>
                        @endforeach


            </select>
        </div>
        
        
        
        
        <div>
            <select class="w-100 mb-2 input-form" name="location" id="location"
            style="height: 32px;">
            
            <option style="border: 1px solid var(--maize-crayola);"  
            class=""
            value="0" selected>Select a location</option>
        
            @foreach ($locations as $location)
                <option style="border: 1px solid var(--maize-crayola);" 
                value="{{ $location->id }}">
                    {{ $location->continent }}
                </option>
            @endforeach
        
        
        
    </select>
</div>





<div class="pt-3">
    
    <input name="price_search" type="range" min="0" max="9999" 
        value="" 
        
        class="slider" id="myRange">
    <p>Max price (<span id="valuta">$</span>): <span id="price_label"></span>
    
    </p>
</div>

<div class=" w-100 d-flex justify-content-center">
    <button type="submit" class="btn bg-burnt-sienna text-white w-100">
        <i class="bi bi-search"></i> 
    </button>
</div>
</form>
</div>


</div>

<div class="col-12 col-md-8 pt-md-4 " >
    
    
    
    @foreach ($announcements as $announcement)
    <x-item_ricerca :announcement="$announcement"/>
    
    
    @endforeach
    
    
    
    
</div>
</div>
</div>


</x-layout>