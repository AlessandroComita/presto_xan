
<x-layout>
    
    <div class="container " style=" margin-top:80px; min-height: 1000px;">
        <div class="row ">
            
            {{-- colonna box ricerche --}}
            <div class="col-12 col-md-4      pb-2 ">
                <div id="box-ricerca" 
                class="bg-charcoal sticky-box  text-white px-3 pb-4 pt-4 mt-1  mb-md-0"
                style="border-radius: 15px; box-shadow: 5px 5px 1px black;"
                >
                
                
                    <div 
                        id="display-results"
                        class=" persian-green ps-2 pe-2 text-center "
                        style="border-radius: 15px; background-color: black;">Search results for <span class="burnt-sienna">{{$q}} </span>
                    </div>
            
            
            
                    <form action="{{ route('filtered_search') }}" method="GET" 
                    class="d-flex  h-100  flex-column justify-content-between w-100">
                    @csrf
                    
                        <div>
                            <input name="q" type="text" value="{{$q}}"
                            class="w-100 mb-2 mt-2 input-form" placeholder="{{$q}}"></input>
                        </div>
                    
                        <div>
                            <select class="w-100 input-form mb-2" name="category" id="category"
                            style="height: 32px;">
                            
                                <option style="border: 1px solid var(--maize-crayola);" 
                                class=""
                                value="0" selected>Select a category</option>
                                
                                
                                @foreach ($categories as $category)                           
                                    <option style="border: 1px solid var(--maize-crayola); font-family: monospace !important;" 
                                    value="{{ $category->id }}"
                                    {{($category->id == $category_search) ? 'selected' : ''}}>
                                    {{$category->name}}
                                    
                                    </option>
                                @endforeach
                            
                            
                            </select>
                        </div>
            
            
            
            
                        <div>
                            <select class="w-100 mb-2 input-form" name="location" id="location"
                            style="height: 32px;">
                            
                            <option style="border: 1px solid var(--maize-crayola);"  
                            class=""
                            value="0" selected>Select a location</option>
                            
                            @foreach ($locations as $location)
                                <option style="border: 1px solid var(--maize-crayola); font-family: monospace !important;" 
                                value="{{ $location->id }}" 
                                {{($location->id == $location_search) ? 'selected' : ''}}>
                                {{ $location->continent }}
                                </option>
                            @endforeach
                        
                        
                        
                            </select>
                        </div>





                        <div class="pt-3">
                            
                            <input name="price_search" type="range" min="0" max="9999" 
                            value="{{$price_search}}" 
                            
                            class="slider" id="myRange">
                            <p>Max price (<span id="valuta">$</span>): <span id="price_label"></span>
                                
                            </p>
                        </div>

                        <div class=" w-100 d-flex justify-content-center">
                            <button type="submit" class="btn bg-burnt-sienna text-white w-100">
                                <i class="bi bi-search"></i> 
                            </button>
                        </div>
                    </form>
                </div>


            </div>


{{-- colonna annunci --}}
            <div class="col-12 col-md-8 pt-md-4 " >

                @if (count($announcements)<=0)
                    <script>document.getElementById('display-results').innerHTML=
                        `
                        
                        No results for <span class="burnt-sienna">{{$q}} </span>
                        
                        `
                    </script>
                @else
                
                    @foreach ($announcements as $announcement)
                    <x-item_ricerca :announcement="$announcement"/>
                    
                    
                    @endforeach
                @endif
                    
                    
                    
                    
            </div>
        </div>
    </div>


</x-layout>