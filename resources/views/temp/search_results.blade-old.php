
<x-layout>
    
    <div class="container  p-0" style="margin-top: 110px; margin-bottom: 310px;">
   


        <div class="row m-0 mt-3 mt-md-5 p-0 justify-content-center">
                <div class="col-12 col-md-10  m-0 p-0 d-flex">
                        <h2 class="titolo2 burnt-sienna">
                            Search results for 
                            <span style="font-weight: bold;" class="persian-green">{{$q}}</span>

                        </h2>
                </div>        
            </div>

                
       {{-- contenitore cards --}}

        <div class="row m-0 mt-2 p-0 justify-content-center pt-2">
            <div class="col-12 d-flex flex-wrap justify-content-center  pb-3 pb-md-0 px-0 px-md-2 mx-0">
                @foreach ($announcements as $announcement)
                    <x-card :announcement='$announcement' />
                @endforeach  
            </div>  
        </div>


                    {{-- {{ $announcements->links() }} --}}

            
                    <div class="d-flex justify-content-center mt-3 p-0" >
                        {!! $announcements->links() !!}
                    </div>


    </div>
 
    
    
    


                {{-- paginazione --}}
                {{-- <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            {{ $announcements->links() }}
                        </div>
                    </div>
                </div>  --}}


</x-layout>