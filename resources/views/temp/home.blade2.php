<x-layout>







<div class="container-fluid mx-0 px-0">


 {{--  central search bar --}}


 
        

            @if (session('announcement.created.success'))
            <div id="avviso" class="d-flex  row w-100 p-0 mt-2 justify-content-center align-items-center   position-absolute">
                    <div class="d-flex align-items-center justify-content-center rounded  w-25 h-100 bg-sandy-brown text-center  charcoal    ">

                        Announcement posted successfully
                    </div>
            </div>
            @endif
        
        
            @if (session('status'))
            <div class="row justify-content-center py-2">
                <div class="col-12 col-md-8 alert alert-success w-100" role="alert">
                    {{ session('status') }}
                </div>
            </div>
            @endif
 


            {{-- messaggio che avvisa l'utente che non ha privilegi sufficienti  --}}

            @if (session('access.denied.revisor.only'))
            <div id="avviso" class=" row  p-0 mt-2 w-100 px-md-5 justify-content-center align-items-center   position-absolute">
                    <div class="col-12 col-md-4 d-flex align-items-center justify-content-center rounded   h-100 bg-sandy-brown text-center  charcoal    ">

                        Accesso non consentito.<br>Servono i privilegi da revisore.
                    </div>
            </div>
            @endif


<!-- Swiper -->
<x-category_swiper />   

</div>


<div class="container-fluid position-relative">

<div class="row">
    <div class="col-12 p-0 m-0">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#2a9d8f" fill-opacity="1" d="M0,96L120,117.3C240,139,480,181,720,213.3C960,245,1200,267,1320,277.3L1440,288L1440,320L1320,320C1200,320,960,320,720,320C480,320,240,320,120,320L0,320Z"></path></svg>
    </div>
</div>




    <div class="row  justify-content-center bg-persian-green position-absolute" style="top: -30px;" >
        <div class="col-12   d-flex">
            <div id="cartello_last_announcement" class="cartello">
                <h2 class="titolo2 charcoal" style="z-index: 1;">
                    
                    {{ __('ui.Last_announcements')}}

                </h2>
            </div>
        </div>        
    </div>
    {{-- contenitore cards --}}

    <div class="row justify-content-center pt-2 bg-persian-green">
        <div class="col-12 d-flex flex-wrap justify-content-center  ">
            @foreach ($announcements as $announcement)
                <x-card :announcement='$announcement' />
            @endforeach  
        </div>  
    </div>

    <div class="row">
        <div class="col-12 p-0 m-0">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#2a9d8f" fill-opacity="1" d="M0,96L120,117.3C240,139,480,181,720,213.3C960,245,1200,267,1320,277.3L1440,288L1440,0L1320,0C1200,0,960,0,720,0C480,0,240,0,120,0L0,0Z"></path></svg>



        </div>
    </div>

</div>






</x-layout>
