<x-layout>

<div class="container mt-3">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Nuovo Annuncio</div>

                <div class="card-body">
                    {{-- <h3>DEBUG:: SECRET {{ $uniqueSecret }} </h3> --}}
         
                    <form action="{{ route('announcement.create') }}"
                        method="POST">
                        @csrf

                        <input 
                          type="hidden"
                          name="uniqueSecret"
                          value="{{ $uniqueSecret }}">



                        <div class="mb-3">
                          <label for="title" class="form-label">Titolo</label>

                          <input id="title" type="text" class="form-control
                          @error('title') is-invalid @enderror"
                          name="title" value="{{ old('title') }}"
                          required autofocus>

                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }} </strong>
                            </span>
                            @enderror


                   


                        </div>
                          
                        <div class="mb-3">
                          <label for="body" class="form-label">Annuncio</label>

                            <textarea
                                id="body"
                                class="form-control 
                                @error('body') is-invalid @enderror"
                                name="body" required autofocus
                                cols="30" rows="10">{{ old('body') }}</textarea>



                          @error('body')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }} </strong>
                            </span>
                          @enderror
                        </div>


                        {{-- elemento componente dropzone --}}

                        <div class="mb-3">
                          <label for="images" class="form-label">Immagini</label>
                            <div class="dropzone" id="drophere"></div>
                            
                          

                              @error('images')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }} </strong>
                                </span>
                              @enderror
                        </div>


                        {{-- campo selezione categoria --}}

                        <div class="row  mb-3" >

                          <div class="col-12 col-sm-6 d-flex align-items-center">
                            <label for="category" class="text-md-right pe-3">Categoria</label>
                            <select class="" name="category" id="category">
                              @foreach ($categories as $category)
                                <option value="{{ $category->id }}"
                                {{old('category') == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>
                              @endforeach
                            </select>

                            @error('category')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }} </strong>
                              </span>
                            @enderror


                          </div>
                        </div>

                            
                        <button type="submit" class="btn btn-primary">Crea</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>


</x-layout>
