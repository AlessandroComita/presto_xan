<div id="box-post" class="container  ">
    <div class="row">
        <div id="box-ragazza-cellulare" class="col-md-6  col-12 d-flex justify-content-end text-end order-2 order-md-1" >
            
            <img class="img-adaptive-height" src="/img/donna3.png">

        </div>
        
        <div id="start-earning-box"
        class="col-md-6   col-12 py-5 order-1 order-md-3  d-flex align-items-between justify-content-evenly flex-column">
            
            
            <p class="josefin2 pt-2 m-0 mb-5 mx-2 bg-maize-crayola charcoal text-center"
                style="border: 10px solid var(--charcoal);
                    box-shadow: 7px -7px 0px var(--persian-green);
                
                "
            
            >START EARNING</p>
            <p class="josefin4 text-white text-center text-md-start">Give to your secondhand goods a second chance</p>
            <p class="josefin4 text-white text-center text-md-start">Sell what you do not use anymore</p>
            <p class="josefin1 text-white text-center text-md-start">Presto</p>
            
            <div class="text-center   d-flex justify-content-center align-items-center justify-content-md-start">
                <a href="{{ route('announcement.new') }}">
                    <button class="bottone bg-charcoal px-2 maize-crayola text-center d-flex justify-content-center align-items-center" style="height: 50px; border-radius: 5px;">
                        Post Free Ad
                        <i class="pt-1 bi bi-plus-circle-fill ms-2" 
                            style="font-size: 1.5rem;"
                        ></i>
                    </button>
                </a>
            </div>
        </div>




    </div>
</div>