<div id="box-brands" class="container-fluid bg-charcoal">
    <div class="row">
        <div class="col-12">

            <!-- Swiper -->
            <div class="swiper swiper2 mt-md-5 mt-3">
                <div class="maize-crayola display-5 ">Our Brands</div>
                <div class="swiper-wrapper swiper-wrapper2 pt-2 pt-md-5 d-flex align-items-center">
                    
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/fiat.png">
                    </div>

                        <div class="swiper-slide swiper-slide2 ">   
                            <img src="/img/brands/chanel.png">
                        </div>
                    <div class="swiper-slide swiper-slide2 ">
                            <img src="/img/brands/adidas.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/nike.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/puma.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/diadora.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/reebok.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/hilti.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/baja.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/bosch.png">
                    </div>

                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/chrysler.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/coca.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/continental.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/dunlop.png">
                    </div>

                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/Jeep.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/johnson.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/oracle.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/suzuki.png">
                    </div>
                    <div class="swiper-slide swiper-slide2 ">   
                        <img src="/img/brands/yamaha.png">
                    </div>



                </div>
                {{-- <div class="swiper-button-next swiper-button-next2"></div>
                <div class="swiper-button-prev swiper-button-prev2"></div>
                <div class="swiper-pagination swiper-pagination2"></div> --}}
            </div>
        </div>
    </div>
</div>