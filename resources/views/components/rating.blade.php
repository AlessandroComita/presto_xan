@if ($rate == 'VERY_UNLIKELY')
        <span> • </span>
        @elseif ($rate == 'UNLIKELY')
        <span> •• </span>
        @elseif ($rate == 'POSSIBLE')
        <span> ••• </span>
        @elseif ($rate == 'LIKELY')
        <span> •••• </span>
        @elseif ($rate == 'VERY_LIKELY')
        <span> ••••• </span>
@endif

