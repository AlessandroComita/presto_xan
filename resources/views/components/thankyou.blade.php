<x-layout>
    
    <div 
        id="box-grazie"
        class="container  " style="margin-top: 150px; height: 500px;">
        <div class="row">
            <div class="col-12 mb-5">
                    <span 
                        id="cartello-thankyou"
                        class="cartello display-5 ">THANK YOU!</span>
            
            </div>

            <div 
                id="paragrafo-grazie"
                class="col-12 maize-crayola text-center mt-5">
                <p><span class="burnt-sienna">{{$informazioni["name"]}}</span>, your application has been sent.</p>
                <p>Our staff will evaluate it and soon contact you back!</p>
            </div>

            <div class="col-12 text-center mt-3 mt-md-5 ">
                <a href="{{ route('home') }} ">
                    <span 
                        id="tasto-thankyou-back"
                        class="bg-maize-crayola py-2 px-3 rounded charcoal" >
                        <nobr>
                            <i class="bi bi-arrow-left-circle-fill"></i> 
                            <span>Go back Home</span>
                            
                        </nobr>
                    </span>
                </a>
            </div>

        </div>
    </div>
</x-layout>