

<div class="container-fluid mb-5 overflow-hidden" >    

    <div class="row ">
        <div id="box-latest" class="col-12 p-0 ">
            <div class="col-12 josefin2-box ps-2">
                Latest Announcements
            </div>

            <div class="row justify-content-center pt-2 ">
                <div class="col-12 pt-2 pb-5 d-flex flex-wrap justify-content-center  ">
                    @foreach ($announcements as $announcement)
                        <x-card :announcement='$announcement' />
                    @endforeach  
                </div>  
            </div>

        </div>
    </div>
</div>




{{-- <div class="container-fluid position-relative">

    <div class="row">
        <div class="col-12 p-0 m-0">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#2a9d8f" fill-opacity="1" d="M0,96L120,117.3C240,139,480,181,720,213.3C960,245,1200,267,1320,277.3L1440,288L1440,320L1320,320C1200,320,960,320,720,320C480,320,240,320,120,320L0,320Z"></path></svg>
        </div>
    </div>
    
    
    
    
       <div class="row  justify-content-center" >
            <div class="col-12   p-0">
                <div id="cartello_last_announcement" class="cartello position-absolute" style="top: 120px; left: 30px;">
                    <h2 class="titolo2 charcoal" style="z-index: 1;">
                        
                        {{ __('ui.Last_announcements')}}
    
                    </h2>
                </div>
            </div>        
        </div>

        
    
        <div class="row justify-content-center pt-2 bg-persian-green">
            <div class="col-12 d-flex flex-wrap justify-content-center  ">
                @foreach ($announcements as $announcement)
                    <x-card :announcement='$announcement' />
                @endforeach  
            </div>  
        </div>
    
        <div class="row">
            <div class="col-12 p-0 m-0">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#2a9d8f" fill-opacity="1" d="M0,96L120,117.3C240,139,480,181,720,213.3C960,245,1200,267,1320,277.3L1440,288L1440,0L1320,0C1200,0,960,0,720,0C480,0,240,0,120,0L0,0Z"></path></svg>
    
    
    
            </div>
        </div>
    
    </div> --}}