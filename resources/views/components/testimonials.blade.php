

<div id="box-testimonianze-large" 
class="container-fluid" style="margin-top: 150px;">






    <div id="swiper-testimonial-large" class="container h-100  ">
        <div class="row h-100">

            <p class=" display-5 charcoal text-end" style="padding-top: 50px; padding-bottom: 0px; margin-bottom: 0px;">Our happy Customers</p>
    <!-- Swiper testimonials large screen-->
    <div class="swiper swiper3 " style="background: transparent;">
        <div class="swiper-wrapper swiper-wrapper3" style="background: transparent;">
        
        <div class="swiper-slide swiper-slide3">
            
            <div class=" h-100 col-md-6 col-12 order-2 order-md-1  d-flex justify-content-start align-items-top align-items-md-center  text-center charcoal d-flex ">
            
                    <i id="bibiquote" class=" bi bi-quote d-flex align-items-start d-flex align-items-start  burnt-sienna"
                    
                        style="text-shadow: 5px 5px var(--persian-green)"
                    ></i>
                    <div class="d-flex flex-column align-items-start justify-content-start">
                        <p id="paragrafo_testimonial" class="" >
                            
                            On time, international shopping, and a good variety. I have been pleased that I can order American plug appliance from a Japanese created grill. Presto did a great job to send me same items of other portals and sometimes less expensive, good quality, and free shipping.

                        </p>
                        <p class="display-6"><i>Mark Duckerberg</i> </p>
                    </div>


            </div>
            <div id="col-testimonials" class="col-md-6 col-12 order-1 order-md-2 overflow-hidden ">

                <div class="mask1">
                    <img src="/img/people/male2.jpg" alt="happy customer" class="img-adaptive overflow-hidden ">
                </div>
                
            </div>





        </div>
        <div class="swiper-slide swiper-slide3">
            

            <div class=" h-100 col-md-6 col-12 order-2 order-md-1  d-flex justify-content-start align-items-top align-items-md-center  text-center charcoal d-flex ">
            
                <i id="bibiquote" class="  bi bi-quote d-flex align-items-start   burnt-sienna"
                
                    style="text-shadow: 5px 5px var(--persian-green)"
                ></i>
                <div class="d-flex flex-column">
                    <p id="paragrafo_testimonial" class="pt-3 " >
                        
                        Full buyer protection. If you open a case they will help you. Every time they solve my problem. Buy with confidence. The biggest shopping site.

                    </p>
                    <p class="display-6"><i>
                        Ofrah Winprey</i></p>
                </div>


        </div>
        <div id="col-testimonials" class="col-md-6 col-12 order-1 order-md-2 overflow-hidden ">

            <div class="mask1">
                <img src="/img/people/female1.png" alt="happy customer" class="img-adaptive overflow-hidden ">
            </div>
            
        </div>



        </div>
        <div class="swiper-slide swiper-slide3">
            


            <div class=" h-100 col-md-6 col-12 order-2 order-md-1  d-flex justify-content-start align-items-top align-items-md-center  text-center charcoal d-flex ">
            
                <i id="bibiquote" class=" bi bi-quote d-flex align-items-start   burnt-sienna"
                
                    style="text-shadow: 5px 5px var(--persian-green)"
                ></i>
                <div class="d-flex flex-column">
                    <p id="paragrafo_testimonial" class="pt-3 " >Excellent customer service, the product came on time. I am really happy with the customer service has been provide to me.</p>
                    <p class="display-6"><i>Wana Del Pay</i></p>
                </div>


        </div>
        <div id="col-testimonials" class="col-md-6 col-12 order-1 order-md-2 overflow-hidden ">

            <div class="mask1">
                <img src="/img/people/female2.png" alt="happy customer" class="img-adaptive overflow-hidden ">
            </div>
            
        </div>




        </div>
        <div class="swiper-slide swiper-slide3">
            

            <div class=" h-100 col-md-6 col-12 order-2 order-md-1  d-flex justify-content-start align-items-top align-items-md-center  text-center charcoal d-flex ">
            
                <i id="bibiquote" class=" bi bi-quote d-flex align-items-start   burnt-sienna"
                
                    style="text-shadow: 5px 5px var(--persian-green)"
                ></i>
                <div class="d-flex flex-column">
                    <p id="paragrafo_testimonial" class="pt-3 " >
                        Very nice finds. Cheap and quality goods. Fast delivery. Sometimes free shipping which is a big plus.
                    </p>
                    <p class="display-6"><i>Alon Mask</i></p>
                </div>


        </div>
        <div id="col-testimonials" class="col-md-6 col-12 order-1 order-md-2 overflow-hidden ">

            <div class="mask1">
                <img src="/img/people/male3.jpg" alt="happy customer" class="img-adaptive overflow-hidden ">
            </div>
            
        </div>


            
        </div>



        <div class="swiper-slide swiper-slide3">
            


            <div class=" h-100 col-md-6 col-12 order-2 order-md-1  d-flex justify-content-start align-items-top align-items-md-center  text-center charcoal d-flex ">
            
                <i id="bibiquote" class="bi bi-quote d-flex align-items-start   burnt-sienna"
                
                    style="text-shadow: 5px 5px var(--persian-green)"
                ></i>
                <div class="d-flex flex-column">
                    <p id="paragrafo_testimonial" class="pt-3 " >
                        
                        I have found the real items, at great prices and backed by Presto. Shipping is great and you are always able to return. You may choose item locations, and shipping/return choices too. Very customer friendly companies, that actually give a hoot about their products and customers.
                    </p>
                    <p class="display-6"><i>Joey Bider</i></p>
                </div>


        </div>
        <div id="col-testimonials" class="col-md-6 col-12 order-1 order-md-2 overflow-hidden ">

            <div class="mask1">
                <img src="/img/people/male1.jpg" alt="happy customer" class="img-adaptive overflow-hidden ">
            </div>
            
        </div>



        </div>

        
        </div>
        {{-- <div class="swiper-button-next swiper-button-next3"></div>
        <div class="swiper-button-prev swiper-button-prev3"></div>
        <div class="swiper-pagination swiper-pagination3"></div> --}}
    </div>





        </div>
    </div>
</div>
    
{{-- swiper testimonial mobile --}}



<div id="box-testimonianze-small" class="container-fluid " style="height: 750px;">
    <div class="row">
        <div class="col-12">
            <p class=" display-5 charcoal text-end">Our happy Customers</p>

        </div>
    </div>
    <div class="row ">


        <div class="swiper swiper4 " style="background: transparent;">
            <div class="swiper-wrapper swiper-wrapper4" style="background: transparent;">
            





                <div class="swiper-slide swiper-slide4">
             
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="mask1s ">
                                    <img src="/img/people/male2s.jpg" alt="happy customer" class="img-adaptive overflow-hidden ">
                                </div>
                    
                            
                                
                                    <i id="bibiquote" class="bi bi-quote  d-flex justify-content-center align-items-top align-items-md-center burnt-sienna"
                                            
                                    style="text-shadow: 5px 5px var(--persian-green)"></i>
                             
                    
                        
                            
                                <p id="paragrafo_testimonial-small-1" class="paragrafo-testimonial-small " >
                                            
                                    On time, international shopping, and a good variety. I have been pleased that I can order American plug appliance from a Japanese created grill. Presto did a great job to send me same items of other portals and sometimes less expensive, good quality, and free shipping.
                                </p>
                                <p class="display-6 nome-testimonial text-center">Mark Duckerberg</p>

                            </div>
                        </div>
                    </div>                                              
                 
                </div>





                <div class="swiper-slide swiper-slide4">
             
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="mask1s ">
                                    <img src="/img/people/female1s.jpg" alt="happy customer" class="img-adaptive overflow-hidden ">
                                </div>
                    
                            
                         
                                <i id="bibiquote" class="bi bi-quote  d-flex justify-content-center align-items-top align-items-md-center burnt-sienna"
                                        
                                style="text-shadow: 5px 5px var(--persian-green)"></i>
                              
                    
                        
                            
                                <p id="paragrafo_testimonial-small-1" class="paragrafo-testimonial-small " >
                                            
                                    Full buyer protection. If you open a case they will help you. Every time they solve my problem. Buy with confidence. The biggest shopping site.
                                </p>
                                <p class="display-6 nome-testimonial text-center">Ofrah Winprey</p>

                            </div>
                        </div>
                    </div>                                              
                 
                </div>

                
                <div class="swiper-slide swiper-slide4">
             
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="mask1s ">
                                    <img src="/img/people/female2s.jpg" alt="happy customer" class="img-adaptive overflow-hidden ">
                                </div>
                    
                            
                         
                                <i id="bibiquote" class="bi bi-quote  d-flex justify-content-center align-items-top align-items-md-center burnt-sienna"
                                        
                                style="text-shadow: 5px 5px var(--persian-green)"></i>
                              
                    
                        
                            
                                <p id="paragrafo_testimonial-small-1" class="paragrafo-testimonial-small " >
                                            
                                    Excellent customer service, the product came on time. I am really happy with the customer service has been provide to me.
                                </p>
                                <p class="display-6 nome-testimonial text-center">Wana Del Pay</p>

                            </div>
                        </div>
                    </div>                                              
                 
                </div>


                <div class="swiper-slide swiper-slide4">
             
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="mask1s ">
                                    <img src="/img/people/male3s.jpg" alt="happy customer" class="img-adaptive overflow-hidden ">
                                </div>
                    
                            
                         
                                <i id="bibiquote" class="bi bi-quote  d-flex justify-content-center align-items-top align-items-md-center burnt-sienna"
                                        
                                style="text-shadow: 5px 5px var(--persian-green)"></i>
                              
                    
                        
                            
                                <p id="paragrafo_testimonial-small-1" class="paragrafo-testimonial-small " >
                                            
                                    Very nice finds. Cheap and quality goods. Fast delivery. Sometimes free shipping which is a big plus.
                                </p>
                                <p class="display-6 nome-testimonial text-center">Alon Mask</p>

                            </div>
                        </div>
                    </div>                                              
                 
                </div>



                <div class="swiper-slide swiper-slide4">
             
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="mask1s ">
                                    <img src="/img/people/male1s.jpg" alt="happy customer" class="img-adaptive overflow-hidden ">
                                </div>
                    
                            
                         
                                <i id="bibiquote" class="bi bi-quote  d-flex justify-content-center align-items-top align-items-md-center burnt-sienna"
                                        
                                style="text-shadow: 5px 5px var(--persian-green)"></i>
                              
                    
                        
                            
                                <p id="paragrafo_testimonial-small-1" class="paragrafo-testimonial-small " >
                                            
                                    I have found the real items, at great prices and backed by Presto. Shipping is great and you are always able to return. You may choose item locations, and shipping/return choices too. Very customer friendly companies, that actually give a hoot about their products and customers.
                                </p>
                                <p class="display-6 nome-testimonial text-center">Joey Bider</p>

                            </div>
                        </div>
                    </div>                                              
                 
                </div>




            </div>
        </div>


        
    </div>
</div>



