<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    


    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PrestoX</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    {{-- animated svg library --}}
    <script src="{{ asset('js/lottie.js') }}"></script>
    


    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bree+Serif&family=Josefin+Sans&family=Kanit&family=Nunito:wght@400;500;700&family=Righteous&family=Syncopate:wght@400;700&family=Zen+Loop:ital@0;1&display=swap" rel="stylesheet">



    {{-- favicon --}}
    <link rel="shortcut icon" href="{{ asset('images/favicon-32x32.png') }}"  type='image/x-icon'>


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-sandy-brown">





            <x-navbar />

            
            <main class="immagine-sfondo-pagina">
                
                <x-wave />

                

                
                {{ $slot }}
                
                






            </main>


                    
                    
            <x-tasto_chat />

   
            <x-footer />


 





</body>
</html>
