
@if (Route::currentRouteName() != 'contact.thankyou' && Route::currentRouteName() != 'contact.send' )
 <x-contact />      
@endif


  

@if (Route::currentRouteName() != 'contact.thankyou' && Route::currentRouteName() != 'contact.send' )
<footer  class="bg-charcoal  w-100">
  <div class="container  pt-3" style="border-top: 1px solid var(--persian-green)">
@else
<footer  class="bg-charcoal  w-100 mt-md-5 ">
  <div class="container pt-3" >
@endif
    <div class="row ">
      <div class="col-md-4 col-12  ">
        
        <p class="burnt-sienna w-50" style="border-bottom: 1px solid var(--persian-green);">ABOUT</p>
        <ul class=" maize-crayola" style="list-style-type:none; padding-left:0px;">
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Contact Us</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">About Us</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Careers</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Presto Stories</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Press</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Presto Wholesale</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Corporate Information</a></li>
          
        </ul>
        
      </div>
      <div class="col-md-4 col-12 ">
        
        <p class="burnt-sienna w-50" style="border-bottom: 1px solid var(--persian-green);">HELP</p>
        
        
        <ul class=" maize-crayola" style="list-style-type:none; padding-left:0px;">
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Payments</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Shipping</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Cancellation & Returns</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">FAQ</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Report infringement</a></li>
          
        </ul>
        
        
      </div>
      <div class="col-md-4 col-12 ">
        
        <p class="burnt-sienna w-50" style="border-bottom: 1px solid var(--persian-green);">POLICY</p>
        
        <ul class=" maize-crayola" style="list-style-type:none; padding-left:0px;">
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Return Policy</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Terms of Use</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Security</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Privacy</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">Sitemap</a></li>
          
          <li class="" ><a href="#" class="footer-item" style="text-decoration: none">EPR Compliance</a></li>
          
          
        </ul>
        
        
      </div>
    </div>
    
    <div class="row text-white text-center pt-3 pb-3" style="font-size: 1rem;">
      <div class=" col-md-3 col-12  ">
        <a href="#" class="footer-item-low  w-100 d-flex flex-wrap justify-content-md-center justify-content-start align-items-center" style="text-decoration: none">
          <i class="bi bi-handbag-fill sandy-brown "></i>
          
          <p class=" p-0 m-0 ps-2 ">Become a Seller</p>
        </a>
      </div>
      
      <div class=" col-md-3 col-12  ">
        <a href="#" class="footer-item-low  w-100 d-flex flex-wrap justify-content-md-center justify-content-start align-items-center" style="text-decoration: none">
        
          <i class="bi bi-bookmark-star-fill sandy-brown"></i>
        
          <p class=" p-0 m-0 ps-2 ">Advertise</p>
        </a>
      </div>
      
      
      <div class=" col-md-3 col-12  ">
        <a href="#" class="footer-item-low  w-100 d-flex flex-wrap justify-content-md-center justify-content-start align-items-center" style="text-decoration: none">
        
          <i class="bi bi-gift-fill sandy-brown"></i>
        
          <p class=" p-0 m-0 ps-2 ">Gift Cards</p>
        </a>
      </div>
      
      
      <div class=" col-md-3 col-12  ">
        <a href="#" class="footer-item-low  w-100 d-flex flex-wrap justify-content-md-center justify-content-start align-items-center" style="text-decoration: none">
        
          <i class="bi bi-question-circle-fill sandy-brown"></i>
        
          <p class=" p-0 m-0 ps-2 ">Help Center</p>
        </a>
      </div>
      
      
      
      
      
      
      
      
    </div>      
    
    
    <div class="row pt-3 pb-2" style="border-top: 1px solid var(--persian-green);">
      
      
      <div class="col-12 col-md-3 d-flex justify-content-md-start justify-content-center mb-md-0 mb-2 align-items-center">
        
        <span class="text-white m-0 p-0 ">&copy; 2007-2022 Presto Inc.</span>
      </div>
      <div class="col-md-6  col-12 d-flex flex-wrap justify-content-center align-items-center">
        <img src="/img/payment/icons8-atm-50.png">
        <img src="/img/payment/icons8-diners-club-50.png">
        <img src="/img/payment/icons8-discover-50.png">
        <img src="/img/payment/icons8-visa-50.png">
        <img src="/img/payment/icons8-google-wallet-50.png">
        <img src="/img/payment/icons8-jcb-50.png">
        <img src="/img/payment/icons8-mastercard-50.png">
        <img src="/img/payment/icons8-paypal-50.png">
        <img src="/img/payment/icons8-stripe-50.png">
        <img src="/img/payment/icons8-unionpay-50.png">



      </div>
      <div class="col-12 col-md-3 pt-3 pt-md-0 d-flex align-items-center justify-content-md-end justify-content-center">
        <a class="text-white ms-3" href="#"><i class="icona-bootstrap-social bi bi-twitter"></i></a>
        
        <a class="text-white ms-3" href="#"><i class="icona-bootstrap-social bi bi-instagram"></i></a>
        
        <a class="text-white ms-3" href="#"><i class="icona-bootstrap-social bi bi-facebook"></i></a>
      </div>
    </div> 
    
    
    
  </div>
</footer>