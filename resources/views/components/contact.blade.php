<div id="box-contatti" class="container-fluid  position-relative" style="margin-top: 450px;">

    <div class="row">
        <div class="col-12 p-0 m-0 ">

            <svg id="wave" style="transform:rotate(0deg); transition: 0.3s" viewBox="0 0 1440 100" version="1.1" xmlns="http://www.w3.org/2000/svg"><defs><linearGradient id="sw-gradient-0" x1="0" x2="0" y1="1" y2="0"><stop stop-color="rgba(233, 196, 106, 1)" offset="0%"></stop><stop stop-color="rgba(233, 196, 106, 1)" offset="100%"></stop></linearGradient></defs><path style="transform:translate(0, 0px); opacity:1" fill="#264653" d="M0,10L240,23.3C480,37,960,63,1440,75C1920,87,2400,83,2880,76.7C3360,70,3840,60,4320,46.7C4800,33,5280,17,5760,11.7C6240,7,6720,13,7200,23.3C7680,33,8160,47,8640,48.3C9120,50,9600,40,10080,36.7C10560,33,11040,37,11520,43.3C12000,50,12480,60,12960,68.3C13440,77,13920,83,14400,75C14880,67,15360,43,15840,28.3C16320,13,16800,7,17280,18.3C17760,30,18240,60,18720,61.7C19200,63,19680,37,20160,25C20640,13,21120,17,21600,30C22080,43,22560,67,23040,68.3C23520,70,24000,50,24480,41.7C24960,33,25440,37,25920,41.7C26400,47,26880,53,27360,50C27840,47,28320,33,28800,33.3C29280,33,29760,47,30240,43.3C30720,40,31200,20,31680,21.7C32160,23,32640,47,33120,50C33600,53,34080,37,34320,28.3L34560,20L34560,100L34320,100C34080,100,33600,100,33120,100C32640,100,32160,100,31680,100C31200,100,30720,100,30240,100C29760,100,29280,100,28800,100C28320,100,27840,100,27360,100C26880,100,26400,100,25920,100C25440,100,24960,100,24480,100C24000,100,23520,100,23040,100C22560,100,22080,100,21600,100C21120,100,20640,100,20160,100C19680,100,19200,100,18720,100C18240,100,17760,100,17280,100C16800,100,16320,100,15840,100C15360,100,14880,100,14400,100C13920,100,13440,100,12960,100C12480,100,12000,100,11520,100C11040,100,10560,100,10080,100C9600,100,9120,100,8640,100C8160,100,7680,100,7200,100C6720,100,6240,100,5760,100C5280,100,4800,100,4320,100C3840,100,3360,100,2880,100C2400,100,1920,100,1440,100C960,100,480,100,240,100L0,100Z"></path></svg>
        </div>






    </div>

    <div class="row riga-contatti    ">


                <div class="col-12 m-0 p-0 col-md-6 bg-charcoal overflow-hidden" >

                    <div id="Onboarding" class="svg-onboarding position-absolute overflow-hidden"
                    style="width: 50%;  bottom: 126px;"
                    ></div>
                </div>
                    
                    <div class="col-12 col-md-6 box-contatti bg-charcoal pt-5 pb-0 pb-md-1  " >
                        <div id="cartello_work-with-us" class="cartello position-absolute mb-5 " style="top: -30px;">
                            <h2 id="titolo-cartello-work" class="titolo2 charcoal" style="z-index: 1;">
                                
                                <nobr>WORK WITH US</nobr>
                            </h2>
                            
                        </div>
                        <p class="sandy-brown text-start" style="z-index: 9999;">Presto is looking for Revisors. <br>Compile the form below if you are interested!</p>
                        <form id="form-contatti" style="z-index: 9999;"
                                method="POST" action="{{ route('contact.send') }}">
                            @csrf
            
                            <div class="row align-items-end">  
                                {{-- <label for="name_contatto" class="py-0 px-2 m-0  col-form-label text-start d-flex align-items-end maize-crayola">{{ __('Name') }}</label> --}}
                            </div>
            
                            <div class="row mb-3">
                                <div class="col-12 m-0 py-0 px-2">  
                                    <input id="name_contatto" type="text" class="form-control input-form-work @error('name_contatto') is-invalid @enderror" name="name_contatto" value="{{ old('name_contatto') }}" required autocomplete="name_contatto" placeholder="Name">
            
                                    @error('name_contatto')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                      
                            <div class="row align-items-end">  
                                {{-- <label for="email_contatto" class="py-0 px-2 m-0  col-form-label text-start d-flex align-items-end maize-crayola">{{ __('Email Address') }}</label> --}}
                            </div>
            
                            <div class="row mb-3">
                                <div class="col-12 m-0 py-0 px-2"> 
                                    <input id="email_contatto" type="email" class="form-control input-form-work @error('email_contatto') is-invalid @enderror" name="email_contatto" value="{{ old('email_contatto') }}" required autocomplete="email" placeholder="Email Address">
            
                                    @error('email_contatto')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        
            
                            
            
                            <div class="row pb-5  ">
                                <div class=" col-12 d-flex justify-content-center px-2">
                                    <button type="submit" class="btn bg-burnt-sienna text-white w-100">
                                        Apply
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

    
                </div>



            </div>
        

       



</div>