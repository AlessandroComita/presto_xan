<div id="statistiche" class="container-fluid p-0 m-0 bg-sandy-brown" style="height: 600px;">

    <div class="row position-relative p-0 m-0 w-100" >
        <div    id="onda-statistiche" 
            class="col-12 p-0 m-0 position-absolute "
            style="top:-60px; z-index: 1;"
            >
    
    
          <img src="/img/onda-statistiche-png.png" 
                class="p-0 m-0 w-100"
                style="width:100%; "> 
    
    
                


        </div>
    </div>



    <div class="row m-0 p-0" >
        <div class="container m-0 p-0 overflow-hidden">
            <div class="row m-0 p-0">

                <div  class=" col-12  pt-5 px-0 mx-0 d-flex justify-content-center flex-column" style="z-index:2;">

                        
                        <div  class="  display-1 text-center charcoal p-0 m-0  "> 
                            <span class=" position-relative">
                                <i id="freccia-sellers" class="bi bi-arrow-up persian-green display-1 position-absolute" style="left: -80px"></i>
                                <span class="contatori" id="numero_sellers"> </span>
                            </span>
                            <p class="display-6 text-center charcoal pt-0 mx-0 px-0">Sellers</p>
                        </div>
               
                    
                        <div  class="  display-1 text-center charcoal p-0 m-0  pt-5"> 
                            <span class=" position-relative">
                                <i id="freccia-sellers" class="bi bi-arrow-up persian-green display-1 position-absolute" style="left: -80px"></i>
                                $
                                <span id="numero_revenue" class="contatori"></span>  
                            </span>
                            <p class="display-6 text-center charcoal pt-0 mx-0 px-0">Monthly Revenue Generated</p>
                        </div>
        

                        <div  class="  display-1 text-center charcoal p-0 m-0 pt-5 "> 
                            <span class=" position-relative">
                                <i id="freccia-sellers" class="bi bi-arrow-up persian-green display-1 position-absolute" style="left: -80px"></i>
                                <span id="numero_datapoints">  
                            </span>
                            <p class="display-6 text-center charcoal pt-0 mx-0 px-0">Data Points Processed Daily</p>
                        </div>
                        

        
        
                </div>

            </div>
        </div>
        
    </div>
</div>