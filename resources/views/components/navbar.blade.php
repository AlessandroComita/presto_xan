<nav class="navbar navbar-expand-md bg-charcoal text-white w-100  fixed-top  "
style="color: white !important; z-index: 999 !important; box-shadow: 0px 1px 1px black;">

    <div class="container position-relative" id="navbar-brand-container">
        <a class="navbar-brand " href="{{ url('/') }}">
            <img src="{{ asset('img/watermark.png') }}" class="" style="width:40px;">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" 
            style="color: transparent;"
            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon d-flex align-items-center">
                <i class="bi bi-caret-down-square text-white"></i>
            </span>
        </button>
        
        <div class="collapse navbar-collapse rounded" id="navbarSupportedContent" >

            <!-- Left Side Of Navbar -->
                 
            {{-- tasto dropdown categorie --}}
            <div class="dropdown ">
                <a class="nav-link dropdown-toggle bg-charcoal ps-3 ps-md-0  pt-md-0 pt-2 pb-md-0 pb-2 d-flex justify-content-start align-items-center " href="#" id="dropdown_categorie" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <span class="nav-link-testo">
                        {{ __('ui.Categories') }}
                    </span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="dropdown_categorie">
                 
                        
                    @if (App::currentLocale()=='es')
               
             
                        @foreach ($categories as $category)
                            <li>
                                <a class="dropdown-item ps-2 " 
                                    href="{{route('public.announcement.category', 
                                    [$category->nome_es, $category->id])}}">
                                    {{ $category->nome_es }}
                                </a>
                            </li>
                        @endforeach
                   

                    @elseif (App::currentLocale()=='it')
                   
                  
                        @foreach ($categories as $category)
                            <li>
                                <a class="dropdown-item ps-2 " 
                                    href="{{route('public.announcement.category', 
                                    [$category->nome_it, $category->id])}}">
                                    {{ $category->nome_it }}
                                </a>
                            </li>
                        @endforeach
                    

                    @else 
                    
                        @foreach ($categories as $category)
                            <li>
                                <a class="dropdown-item ps-2 " 
                                    href="{{route('public.announcement.category', 
                                    [$category->name, $category->id])}}">
                                    {{ $category->name }}
                                </a>
                            </li>
                        @endforeach
                    @endif





                </ul>

            


            </div>

            <x-searchbar />
            <x-button_add />

            


   
   
          



    <!-- Right Side Of Navbar -->







            <ul class="navbar-nav ms-auto">

     

            {{-- bandierine linguaggio --}}
            
   
          
            <div class="dropdown d-flex align-items-md-center flex-column flex-md-row me-md-2 w-100 py-2 bg-charcoal" >


                <a class="btn  position-relative btn-locale dropdown-toggle w-100 text-start ps-3 ps-md-0 d-flex justify-content-start align-items-center" href="#" role="button" id="dropdown_locale" data-bs-toggle="dropdown" aria-expanded="false">

                    @if (App::currentLocale()=='it')
                        <i class="flag-icon flag-icon-it "></i>
                    @elseif (App::currentLocale()=='es')
                        <i class="flag-icon flag-icon-es "></i>
                    @elseif (App::currentLocale()=='en')
                        <i class="flag-icon flag-icon-gb "></i>
                    @else
                        <i class="bi bi-flag "></i>
                    @endif
                  </a>


                <ul class="dropdown-menu dropdown-menu-md-end position-absolute " aria-labelledby="dropdown_locale" >

                    <li>                   
                        <form action="{{ route('locale','en') }}" method="POST" class="w-100 text-end " >
                            @csrf
                            <button type="submit" class="dropdown-item text-start " style="width: 100% !important; background-color: transparent; border: none;">

                              
                                <span class="flag-icon flag-icon-gb "></span><span class="ps-2">English</span>

                            </button>      
                          </form>
                    </li>



                    <li>
                        <form action="{{ route('locale','it')}}" method="POST" class="w-100 text-end" >
                            @csrf
                            <button type="submit" class="dropdown-item  text-start " style="width: 100% !important; background-color: transparent; border: none;">

                              
                                <span class="flag-icon flag-icon-it "></span><span class="ps-2">Italian</span>

                            </button>      
                          </form>
                    </li>



                    <li>                             
                        <form action="{{ route('locale','es') }}" method="POST" class="w-100 text-end" >
                            @csrf
                            <button type="submit" class="dropdown-item  text-start " style="width: 100% !important; background-color: transparent; border: none;">

                              
                                <span class="flag-icon flag-icon-es "></span>
                                <span class="ps-2">Spanish</span>
                              

                            </button>      
                          </form>
                    </li>


                </ul>
            </div>


      


                <!-- Authentication Links -->
                
                {{-- dropdown utente non loggato --}}

            @guest

            <div class="dropdown d-flex align-items-center" >


                <a class="btn  btn-locale dropdown-toggle w-100 text-start ps-3 ps-md-0 pb-2 pb-md-0 rounded-bottom d-flex justify-content-start 
                justify-content-md-center
                align-items-center" href="#" role="button" id="dropdown_guest" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="bi bi-person-circle d-flex justify-content-center align-items-center" style="font-size:x-large;"></i>
                  </a>

                <ul class="dropdown-menu dropdown-menu-start position-absolute" aria-labelledby="dropdown_guest" style="z-index: 999999;">
                    @if (Route::has('login'))
                        <li class="">
                            <a class="dropdown-item ps-2" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif
                    @if (Route::has('register'))
                        <li class="">
                            <a class="dropdown-item ps-2 " href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif

                </ul>
            </div>




                @else
               {{-- dropdown utente loggato --}}

                <div class="dropdown d-flex align-items-center justify-content-md-center justify-content-start bg-charcoal rounded-bottom" >
  


                    <a class="nav-link dropdown-toggle bg-charcoal d-flex align-items-center  justify-content-start pe-0 ps-3 pb-2 pb-md-0 ps-md-0 rounded-bottom" 
                        
                    
                    href="#" id="dropdown_utente" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <span class="nav-link-testo">
                            {{ Auth::user()->name }}
                        </span>
                        @if(Auth::user()->is_revisor && \App\Models\Announcement::ToBeRevisionedCount())                                 
                            <span class="ms-1 badge badge-pill bg-maize-crayola charcoal">
                                {{ 
                                    \App\Models\Announcement::ToBeRevisionedCount() 
                                }}
                            </span>
                        @endif
                    </a>

                    <ul class="dropdown-menu position-absolute" aria-labelledby="dropdownMenuButton1">
                        <li>
                            @if(Auth::user()->is_revisor)
                        
                            <a class="dropdown-item pe-2 d-flex align-items-center justify-content-start ps-2" href=" {{ route('home.revisor') }}">
                                    Revisor home
                            </a>
                        
                            @endif
                        </li>




                      <li>
                        <a class="dropdown-item ps-2"
                            style="z-index: 99999;"                        
                            href="{{ route('announcement.new') }}">
                            Post Free Ad
                        </a>  


                      </li>
                      <li>
                                <a class="dropdown-item ps-2" 
                                    style="z-index: 99999;"
                                    href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                 </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>

                      </li>

                    </ul>
                  </div>
                  
               
            @endguest
            </ul>
        </div>
    </div>
</nav>