<div class="container-fluid my-5 ">







  <div class="row">
    <div id="discover-bar" class="col-12 p-0">

      <span class="col-12 josefin2-box ps-2 py-2 pe-2">
        Discover the world of Presto</span>
      
      <div class="swiper swiper1 "> 
         
      
        <div class="swiper-wrapper swiper-wrapper1 position-relative py-3"  >
          
        @foreach ($categories as $category)
        
          <div class="swiper-slide swiper-slide1">
            <a style="text-decoration: none !important; color: white !important;" href="{{route('public.announcement.category', 
              [$category->name, $category->id])}}">
              <img src="{{asset($category->path)}}" />
              <div 
                  class="label-categorie text-center bg-burnt-sienna position-absolute w-100"
              >{{$category->name}}</div>
            </a>
          </div>
        
        @endforeach
  
  
          
        </div>
        {{-- <div class="swiper-pagination swiper-pagination1"></div> --}}
      </div>

    </div>
  </div>
 

  </div>