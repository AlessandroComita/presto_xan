

<div 
    id="container-welcome "
    class="container  mt-5 " style="overflow: hidden;">

<div class="row position-relative">
    <div class="col-12"> 
        @if (session('announcement.created.success')) 
        <div id="avviso" class=" d-flex  row w-100 p-0 mt-2 justify-content-center align-items-center position-absolute">
            <div
                id="box-avviso-annuncio-postato" 
                class="avviso-sovraimpresso  m-5 d-flex align-items-center justify-content-center rounded   h-100  text-center  charcoal">

                Announcement posted successfully

            </div>
        </div>
        @endif 
    
    </div>
</div>

    <div class="row ">
        <div class="col-md-1 col-12 ">
                

            
        </div>
        <div 
            id="col-ecommerce"
            class="col-12 col-md-6  d-flex justify-content-end" >

            <div id="Ecommerce" class=""
                style="height: 95%; width: 95%;">
            </div> 

        </div>

        <div
            id="riga-welcome" 
            class="col-12  col-md-5 d-flex align-items-md-start
        align-items-center justify-content-center flex-column position-relative "
            
        >
                

                <p id="paragrafo-world" class="josefin2 text-center m-0 p-0"><nobr> A world of products</nobr></p>
                <p id="paragrafo-just" class="josefin2l text-center m-0 p-0">Just a click away</p>
                <p id="paragrafo-presto" class="josefin1 text-center m-0 p-0">Presto</p>
        </div>

    

    </div>
</div>
