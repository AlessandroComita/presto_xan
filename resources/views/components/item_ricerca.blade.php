<a class="link-dettaglio" style="color: black"
    href="{{ route('detail', ['id' => $announcement->id])}}">
    <div class="container  m-0 p-0 py-2 my-2  py-md-0 px-3" style="border-radius: 15px; border: 1px solid var(--maize-crayola); background-image: linear-gradient(to bottom, rgba(255,255,255,0.8), rgba(255,255,255,0.3));"  >
        <div class="row   h-100 p-0 m-0 d-flex align-items-between  ">
            <div class="col-12  col-md-4  p-0 py-md-3 m-0 my-1 my-md-0 d-flex align-items-center ">
                
                @if ($announcement->images->first() == null)
                <img class="p-0"
                src={{asset('img/not_available.png')}}>
                @else

                <img class="immagine-annuncio-ricerca  "
                    
                    src="{{ $announcement->images->first()->getUrl(300, 200) }}">
                @endif

            </div>
            <div class="col-12  col-md-8  p-0 m-0 my-1 my-md-2  " >
                <div 
                id="item-ricerca-dati"
                class=" px-2 w-100 py-2 ms-md-2 overflow-hidden d-flex align-items-between justify-content-between flex-column" style="font-size: 0.8rem; height: 100%; border-left: 1px solid var(--persian-green);">
                    {{-- <p>{{$announcement->id}}</p> --}}
                    <p class="charcoal  m-0 p-0   " 
                        
                                style= "font-size: 1.4rem; font-weight:bold;"
                        >{{ $announcement->title }} </p>

                        
                        <p class="paragrafo-item charcoal m-0 p-0"
                            
                        
                        >{{ substr($announcement->body, 0, 200) }}...</p>

                        <div class=" p-0 m-0">
                            
                                <p 
                                    class="mb-0"
                                style="font-size: 1.2rem; font-family: monospace; font-weight: bolder;">
                                    <nobr>
                                        $ {{ $announcement->price }} 
                                    </nobr>
                                </p>
                            
                                <nobr>
                                <i class="bi bi-tag-fill persian-green "></i>
                                {{ $announcement->category->name }}
                                </nobr>
                            
                                
                                
                                <nobr>
                                <i class="bi bi-geo-alt-fill persian-green ms-1"></i>
                                {{$announcement->location->continent}}
                                </nobr>
                                
                        </div>    
                            
                            
                    
                </div>


            </div>
        </div>
    </div>
</a>