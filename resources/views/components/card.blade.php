
    @if ($announcement)
    <a href="{{ route('detail', ['id' => $announcement->id])}}">
        <div class="container position-relative carta  mb-2 mx-2">
            <div class="row  justify-content-center">
                <div class="cornice-immagine-annuncio">
                    @if ($announcement->images->first() == null)
                    <img class="immagine-annuncio p-0"
                    src={{asset('img/not_available.png')}}>
                     @else

                    <img class="immagine-annuncio p-0"
                        src="{{ $announcement->images->first()->getUrl(300, 200) }}">
                    @endif
                </div>

                {{-- box categoria --}}
        
                              
                        
                 


          

                   
           
                
                
                
                
                <p class="charcoal mt-4  m-0 p-0 px-3 "
                
                        style= "font-size: 1rem; font-weight:bold;"
                >{{ substr($announcement->title, 0, 50) }}</p>

                
                <p class="charcoal m-0 p-0 px-4 pt-2"
                
                style= "font-size: 0.8rem; "
                >{{ substr($announcement->body, 0, 100) }}...</p>
                

                {{-- riga box prezzo e categoria --}}
                <div class="riga-categoria-prezzo position-absolute
                                    d-flex justify-content-between m-0 p-0
                "
                                style="top: 48%;"
                
                >
                    <p class=" p-0 m-0 px-2 bg-burnt-sienna text-white rounded-end">{{ $announcement->price }} $</p>


                    <a class=""
                    href="{{ route('public.announcement.category', 
                        [$announcement->category->name, $announcement->category->id])   }}"> 

                        <p class=" p-0 m-0 px-2 bg-burnt-sienna text-white rounded-start">
                            

                            @if (App::currentLocale()=='it')
                            {{ $announcement->category->nome_it }} 
                            @elseif (App::currentLocale()=='es')
                                {{ $announcement->category->nome_es }} 
                            @elseif (App::currentLocale()=='en')
                                {{ $announcement->category->name }} 
                            @else
                                {{ $announcement->category->name }} 
                            @endif


                        </p>
                    </a>


                </div>
                
                <div class="footer-card p-0 p-2 d-flex justify-content-between position-absolute bottom-0">
                    <div class=""><h6 class="m-0">{{ $announcement->user->name }}</h6></div>
                        <div class="d-flex justify-content-end">
                            <div class="d-flex align-items-end">
                                <h6 class="testo-data-ora m-0">{{ $announcement->created_at->format('H:i:s') }} | </h6>
                            </div>
                            <div class=" d-flex align-items-end">
                                <h6 class="testo-data-ora m-0 ms-1"> {{ $announcement->created_at->format('d/m/y') }}</h6>
                            </div>
                        </div>
                </div>

                {{-- CHIUSURA ROW    --}}
            </div>
        </div>
    </a>
@endif