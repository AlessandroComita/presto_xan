@if (Route::currentRouteName() != 'home.revisor')

<div class="container bg-charcoal">
    <div class="row position-relative">
        <div class="col-12">
            
            <form action="{{ route('search') }}" method="GET" class="w-100 d-flex align-items-center justify-content-center"  style="background: transparent;">
                @csrf
                <input name="q" class="w-100 ps-md-3 ps-3 input-form " type="text"  placeholder="Search"
                style="height:35px; font-family: monospace !important;"
                />
                <button class="ms-1 btn text-center burnt-sienna
                    d-flex align-items-center justify-content-center"
                    type="submit"
                    style="height: 30px; width:30px; position: absolute; right: 20px;">                
                        <i class="bi bi-search d-flex align-items-center justify-content-center"></i>             
                </button>
            </form>  


        </div>
    </div>
</div>
@endif