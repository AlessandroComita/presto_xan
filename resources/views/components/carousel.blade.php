<div id="carouselExampleIndicators" 
    class="carousel slide d-flex align-items-center justify-content-center pe-md-4" 
    data-bs-ride="carousel" 
    style="width: 100%;">
  <div class="carousel-indicators">
    @for ($i = 0; $i < (count($announcement->images)); $i++)
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{$i}}" {{$i == 0 ? 'class=active' : ''}}  aria-current="true" aria-label="Slide {{$i+1}}"></button>
    @endfor    
  </div>

  <div class="carousel-inner py-1">
    @for ($i = 0; $i < (count($announcement->images)); $i++)
    <div class="carousel-item {{$i == 0 ? 'active' : ''}}">
      <img src="{{ $announcement->images[$i]->getUrl(400, 300)}}" 
        class="d-block w-100" alt="Slide {{$i}}"
          style="border-radius: 15px;">
    </div>
    @endfor
  </div>

  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>