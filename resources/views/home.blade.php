<x-layout>



<x-welcome />






 {{--  messaggio esito annuncio --}}
<div class="container-fluid">

         
        
        
            @if (session('status'))
            <div class="row justify-content-center py-2">
                <div class="col-12 col-md-8 alert alert-success w-100" role="alert" >
                    {{ session('status') }}
                </div>
            </div>
            @endif
 


            {{-- messaggio che avvisa l'utente che non ha privilegi sufficienti  --}}

            @if (session('access.denied.revisor.only'))
            <div id="avviso" class=" row  p-0 mt-2 w-100 px-md-5 justify-content-center align-items-center   position-absolute" style="top:10%;"6>
                    <div class="avviso-sovraimpresso col-12 col-md-4 d-flex align-items-center justify-content-center rounded   h-100  text-center  charcoal    ">

                        Accesso non consentito.<br>Servono i privilegi da revisore.
                    </div>
            </div>
            @endif
</div>


<!-- Swiper -->
<x-category_swiper />   




<x-last_announcements :announcements="$announcements"/>

<x-box_post />




<x-swiper_brands />

            


<x-beautiful />


<x-statistiche /> 


<x-testimonials />



</x-layout>
