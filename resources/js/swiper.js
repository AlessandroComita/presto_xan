  // import Swiper bundle with all modules installed
  import Swiper from 'swiper/bundle';

  // import styles bundle
  import 'swiper/css/bundle';

// carosello categorie
  var swiper1 = new Swiper(".swiper1", {
    
    autoplay: {
      delay: 5000,
    },
    effect: "coverflow",
    grabCursor: true,
    centeredSlides: true,
    loop: 'true',
    slidesPerView: '4',
    initialSlide: "1",
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
    pagination: {
      el: ".swiper-pagination1",
    },
  });

  // carosello brands
  var swiper2 = new Swiper(".swiper2", {
    autoplay: {
      delay: 1000,
    },
    slidesPerView: 5,
    spaceBetween: 30,
    slidesPerGroup: 1,
    loop: true,
    loopFillGroupWithBlank: true,

     // Responsive breakpoints
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 4,
        spaceBetween: 30
      },
      // when window width is >= 640px
      640: {
        slidesPerView: 5,
        spaceBetween: 40
      }
    },


    pagination: {
      el: ".swiper-pagination2",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next2",
      prevEl: ".swiper-button-prev2",
    }
  });



  var swiper3 = new Swiper(".swiper3", {
    autoplay: {
      delay: 10000,
    },
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    pagination: {
      el: ".swiper-pagination3",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next3",
      prevEl: ".swiper-button-prev3",
    },
  });


  var swiper4 = new Swiper(".swiper4", {
    autoplay: {
      delay: 10000,
    },
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    pagination: {
      el: ".swiper-pagination4",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next4",
      prevEl: ".swiper-button-prev4",
    },
  });