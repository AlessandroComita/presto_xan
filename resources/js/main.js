if (document.getElementById("avviso"))
{
  const eAvviso = document.getElementById("avviso");
  
  
  
  // ritardo prima della comparsa
  const myTimeout0 = setTimeout(appear, 3000);
  
  function appear() {
    eAvviso.classList.add("compari");
  }
  
  // il box del messaggio della creazione dell'annuncio sparisce con una dissolvenza
  const myTimeout = setTimeout(disappear, 7000);
  
  function disappear() {
    eAvviso.classList.remove("compari");
    eAvviso.classList.add("sparisci");
  }
  
  // il box del messaggio della creazione dell'annuncio viene eliminato fisicamente dal flusso della pagina
  const myTimeout2 = setTimeout(destroy, 9000);
  
  function destroy() {
    eAvviso.classList.add("d-none");
  }
  
}




// controlla se ci troviamo  nella pagina di inserimento annuncio
if (document.getElementById("price"))
{
  
  // cattura casella «prezzo» del form
  var e_price = document.getElementById('price');
  
  
  // imposta un numero massimo di caratteri inseribili nella casella prezzo del form
  var maxPriceChars = 13; 
  
  // se l'utente inserisce nella casella del prezzo un numero di caratteri superiore
  // a maxPriceChars, toglie caratteri di troppo, così da bloccare
  // l'inserimento al massimo previsto
  e_price.oninput = function () 
  {
    if (this.value.length > maxPriceChars) 
    {
      this.value = this.value.slice(0, maxPriceChars); 
    }
  }
}

if (document.getElementById("myRange") && document.getElementById("price_label"))
{
  var slider = document.getElementById("myRange");
  var output = document.getElementById("price_label");
  
  output.innerHTML = slider.value;
  
  if (slider.value == 0 || slider.value == 9999)
  {
    output.innerHTML = 'All prices';
  }
  
  slider.oninput = function() 
  {
    if (slider.value == 0 || slider.value == 9999)
    {
      output.innerHTML = 'All prices';
    }
    else
    {
      output.innerHTML = this.value;
    }
    
  }
}




// gestione contatori box statistiche

if (document.getElementById('numero_sellers'))
{
  let counter1 = 0;
  let counter2 = 0;
  let counter3 = 0;



  const e_numero_sellers = document.getElementById('numero_sellers');
  e_numero_sellers.innerHTML = counter1;

  const e_numero_revenue = document.getElementById('numero_revenue');
  e_numero_revenue.innerHTML = counter2;

  const e_numero_datapoints = document.getElementById('numero_datapoints');
  e_numero_datapoints.innerHTML = counter3;


  const observer1 = new IntersectionObserver(handleIntersection1);

  function handleIntersection1(entries)
  {
    entries.forEach(entry =>
      {
        if (entry.isIntersecting)
        {
          let interval = setInterval(() => 
          {
            
            if (counter1<1000000)
            {

              counter1+=11111;
              e_numero_sellers.innerHTML = counter1;
              if (counter1>=1001011)
              {
                e_numero_sellers.innerHTML = '1,000,000+';
              }
            }
          }, 1);
        }
          
      })
      
    } 
      
    observer1.observe(e_numero_sellers);



    const observer2 = new IntersectionObserver(handleIntersection2);

    function handleIntersection2(entries)
    {
      entries.forEach(entry =>
        {
          if (entry.isIntersecting)
          {
            let interval = setInterval(() => 
            {
              
              if (counter2<4000000)
              {

                counter2+=31111;
                e_numero_revenue.innerHTML = counter2;
                if (counter2>=4000000)
                {
                  e_numero_revenue.innerHTML = '4,000,000+';
                }
              }
            }, 1);
          }
              
        })
        
      } 
        
      observer2.observe(e_numero_revenue);



      const observer3 = new IntersectionObserver(handleIntersection3);

      function handleIntersection3(entries)
      {
        entries.forEach(entry =>
          {
            if (entry.isIntersecting)
            {
              let interval = setInterval(() => 
              {
                
                if (counter3<2000000)
                {

                  counter3+=21111;
                  e_numero_datapoints.innerHTML = counter3;
                  if (counter3>=2000000)
                  {
                    e_numero_datapoints.innerHTML = '2,000,000+';
                  }
                }
              }, 1);
            }
                
          })
          
        } 
          
        observer3.observe(e_numero_datapoints);
}

    
      
      
      