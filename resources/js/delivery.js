// animazione SVG del work with us (barca)

if (document.getElementById('Onboarding'))
{
    var animation = bodymovin.loadAnimation({
        container: document.getElementById('Onboarding'),
            
        // Set your ID to something that you'll associate with the animation you're using //
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: '/img/Onboarding.json'
            
        // Make sure your path has the same filename as your animated     SVG's JSON file //
        });
}



// animazione SVG della welcome (monolite ecommerce)

if (document.getElementById('Ecommerce'))
{
    var animation2 = bodymovin.loadAnimation({
        container: document.getElementById('Ecommerce'),
            
        // Set your ID to something that you'll associate with the animation you're using //
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: '/img/e-commerce.json'
            
        // Make sure your path has the same filename as your animated     SVG's JSON file //
        })
}
